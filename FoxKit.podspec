#
#  Be sure to run `pod spec lint FoxKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "FoxKit"
  s.version      = "0.0.4"
  s.summary      = "just for fun."

  s.description  = <<-DESC
                   A longer description of FoxKit in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "https://github.com/foxsofter/FoxKit"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "foxsofter" => "foxsofter@gmail.com" }
  s.platform     = :ios, "7.0"

  s.ios.deployment_target = "7.0"

  s.source = { :git => "https://github.com/foxsofter/FoxKit.git", :tag => "#{s.version}", :submodules => true }

  s.source_files  = 'FoxKit/FoxKit.h'

  s.requires_arc = true

  s.subspec 'Additions' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.source_files = 'FoxKit/Additions/*.{h,m}'
  end
  s.subspec 'Controls' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.dependency 'FoxKit/Additions'
    ss.dependency 'SDiPhoneVersion'
    ss.dependency 'Masonry'
    ss.dependency 'AFNetworking'
    ss.dependency 'TTTAttributedLabel'
    ss.dependency 'JXBAdPageView'
    ss.dependency 'NUI'
    ss.source_files = 'FoxKit/Controls/*.{h,m}','FoxKit/Controls/ControlStyles/*.{h,m}','FoxKit/Controls/FXPopupView/*.{h,m}','FoxKit/Controls/FXRating/*.{h,m}','FoxKit/Controls/FXSegmented/*.{h,m}','FoxKit/Controls/SphereMenu/*.{h,m}','FoxKit/Controls/SwipeView/*.{h,m}','FoxKit/Controls/KxMenu/*.{h,m}','FoxKit/Controls/FXBarButtonItem/*.{h,m}','FoxKit/Controls/FXCollectionViewBoxLayout/*.{h,m}','FoxKit/Controls/FXAssetsPickerController/*.{h,m}'
    ss.resources    = "FoxKit/Controls/FXAssetsPickerController/*.{bundle,xib}"
  end
  s.subspec 'Mvvm' do |ss|
    ss.ios.deployment_target = '7.0'
    ss.dependency 'FoxKit/Additions'
    ss.dependency 'FoxKit/Controls'
    ss.dependency 'Mantle', '1.5.4'
    ss.dependency 'AFNetworking'
    ss.dependency 'CocoaSecurity', '~> 1.2.4'
    ss.dependency 'ReactiveCocoa'
    ss.dependency 'ReactiveViewModel'
    ss.dependency 'KVNProgress'
    ss.dependency 'MJRefresh'
    ss.dependency 'MagicalRecord'
    ss.source_files = 'FoxKit/Mvvm/*.{h,m}'
    ss.resources = "FoxKit/Mvvm/*.{xcdatamodeld}"
  end
end
