//
//  FXClientModelService.h
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXModelService.h"
#import "FXClientPassportModel.h"
#import "FXClientCredentialModel.h"

@interface FXClientModelService : FXModelService

/**
 *  @author foxsofter, 15-10-31 20:10:51
 *
 *  @brief  注册客户端
 *
 *  @return
 */
- (RACSignal*)signupClient;

/**
 *  @author foxsofter, 15-10-31 20:10:10
 *
 *  @brief  获取客户端token
 *
 *  @return
 */
- (RACSignal*)getClientToken;

@end
