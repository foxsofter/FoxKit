//
//  FXClientModelService.m
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXClientModelService.h"
#import "UIDevice+Addition.h"

@implementation FXClientModelService

//  /oauth/client
- (RACSignal*)signupClient {
  UIDevice* device = [UIDevice currentDevice];
  NSDictionary* parameters = @{
    @"type" : @"user-ios",
    @"deviceName" : device.name,
    @"deviceIdentify" : device.identifierForVendor.UUIDString,
    @"version" : @"1.0",
    @"redirectURI" : @"http://letsdo.sports",
    @"deviceVersion" : [UIDevice platformString],
    @"deviceOsVersion" : device.systemVersion,
  };

  return [self.client rac_POST:@"/oauth/client"
                    parameters:parameters
                   resultClass:[FXClientPassportModel class]];
}

// /oauth/token
//- (RACSignal *)getClientToken {
//  return [self.client rac_POST:@"/oauth/client"
//                    parameters:parameters
//                   resultClass:[FXClientCredentialModel class]];
//}

@end
