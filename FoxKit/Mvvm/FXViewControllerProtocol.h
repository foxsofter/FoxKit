//
//  FXViewControllerProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FXViewModelProtocol;

@protocol FXViewControllerProtocol <NSObject>

@required

- (instancetype)initWithViewModel:(id<FXViewModelProtocol>)viewModel;

@property (nonatomic, strong, readonly) id<FXViewModelProtocol> viewModel;

@end