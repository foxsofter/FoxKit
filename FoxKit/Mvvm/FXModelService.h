//
//  FXModelService.h
//  FoxKit
//
//  Created by foxsofter on 15/10/12.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXModelServiceProtocol.h"

@interface FXModelService : NSObject<FXModelServiceProtocol>

@end
