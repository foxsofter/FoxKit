//
//  FXModelService.m
//  FoxKit
//
//  Created by foxsofter on 15/10/12.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXModelService.h"

@interface FXModelService ()

@property (nonatomic, strong, readwrite) FXClient* client;

@end

@implementation FXModelService

- (FXClient*)client {
  if (!_client) {
    _client = [FXClient sharedInstance];
  }
  return _client;
}

@end
