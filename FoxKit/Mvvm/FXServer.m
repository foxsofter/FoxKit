//
//  FXServer.m
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXServer.h"

@interface FXServer ()

@property (nonatomic, strong) AFNetworkReachabilityManager* reachability;

@end

@implementation FXServer

#pragma mark - life cycle

+ (instancetype)serverWithBaseURL:(NSString*)baseURL {
  return [[FXServer alloc] initWithBaseURL:baseURL];
}

- (instancetype)initWithBaseURL:(NSString*)baseURL {
  self = [super init];
  if (self) {
    _reachability = [AFNetworkReachabilityManager managerForDomain:baseURL];
    _baseURL = [NSURL URLWithString:baseURL];
  }
  return self;
}

#pragma mark - public methods

- (void)startMonitoring {
  [_reachability startMonitoring];
}

- (void)stopMonitoring {
  [_reachability stopMonitoring];
}

#pragma mark - getters & setters

- (AFNetworkReachabilityStatus)networkReachabilityStatus {
  return _reachability.networkReachabilityStatus;
}

- (BOOL)isReachability {
  return _reachability.reachable;
}

- (BOOL)isReachableViaWWAN {
  return _reachability.reachableViaWWAN;
}

- (BOOL)isReachableViaWiFi {
  return _reachability.reachableViaWiFi;
}

@end
