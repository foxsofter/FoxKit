//
//  FXErrorDescriptionModel.m
//  FoxKit
//
//  Created by fox softer on 15/11/1.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXErrorDescriptionModel.h"
#import "MagicalRecord/MagicalRecord.h"
#import "ErrorDescription.h"

@implementation FXErrorDescriptionModel

@synthesize type;
@synthesize code;
@synthesize error;

+ (void)initialize {
  [MagicalRecord setupCoreDataStackWithStoreNamed:@"Model.sqlite"];
}

+ (void)finalize {
  [MagicalRecord cleanUp];
}

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
  return @{
    @"type" : @"type",
    @"code" : @"code",
    @"error" : @"error",
  };
}

- (NSString*)description {
  return [NSString stringWithFormat:@"<%@, %@, %@, %@>", self.class, self.type,
                                    self.code, self.error];
}

+ (NSString*)errorLocaleDescriptionByCode:(NSNumber*)code {
  FXErrorDescriptionModel* match = nil;
  NSArray<FXErrorDescriptionModel*>* errors =
      [FXErrorDescriptionModel errorDescriptions];
  for (FXErrorDescriptionModel* error in errors) {
    if ([error.code isEqualToNumber:code]) {
      match = error;
      break;
    }
  }
  if (nil == match) {
    return nil;
  }
  NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
  if ([language containsString:@"en"]) {
    return match.error.firstObject;
  } else {
    return match.error[2];
  }
}

static NSArray<FXErrorDescriptionModel*>* _errorDescriptions;

+ (NSArray<FXErrorDescriptionModel*>*)errorDescriptions {
  if (nil == _errorDescriptions) {
    NSArray* cdErrorDescriptions = [ErrorDescription MR_findAll];
    NSMutableArray* mtErrorDescriptions = [NSMutableArray array];
    for (ErrorDescription* cdError in cdErrorDescriptions) {
      FXErrorDescriptionModel* mtError = [[FXErrorDescriptionModel alloc] init];
      mtError.type = cdError.type;
      mtError.code = cdError.code;
      mtError.error = cdError.error;
      [mtErrorDescriptions addObject:mtError];
    }
    _errorDescriptions = mtErrorDescriptions;
  }
  return _errorDescriptions;
}

+ (void)setErrorDescriptions:
    (NSArray<FXErrorDescriptionModel*>*)errorDescriptions {
  _errorDescriptions = errorDescriptions;
  [ErrorDescription MR_truncateAll];
  for (FXErrorDescriptionModel* mtError in errorDescriptions) {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext* localContext) {
      ErrorDescription* error =
          [ErrorDescription MR_createEntityInContext:localContext];
      error.type = mtError.type;
      error.code = mtError.code;
      error.error = mtError.error;
    }];
  }
  _errorDescriptions = nil;
}

@end
