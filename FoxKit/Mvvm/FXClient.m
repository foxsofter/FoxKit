//
//  FXClient.m
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXClient.h"
#import "FXServer.h"
#import "UIDevice+Addition.h"
#import "FXClientPassportModel.h"
#import "FXClientCredentialModel.h"
#import "FXClientModelService.h"

NSString* const FXServerBaseUrl = @"http://apitest.yuedong.co";
NSString* const FXServerOAuth2ClientSignupUrl = @"/oauth/client";
NSString* const FXServerOAuth2ClientSigninUrl = @"/oauth/token";

NSString* const FXClientErrorDomain = @"FXClientErrorDomain";
NSString* const FXResponseObjectErrorKey = @"responseObject";

NSString* const AFOAuth2ErrorDomain = @"com.alamofire.networking.oauth2.error";

NSString* const kAFOAuthCodeGrantType = @"authorization_code";
NSString* const kAFOAuthClientCredentialsGrantType = @"client_credentials";
NSString* const kAFOAuthPasswordCredentialsGrantType = @"password";
NSString* const kAFOAuthRefreshGrantType = @"refresh_token";

NSString* const kAFOAuth2CredentialServiceName = @"AFOAuthCredentialService";

@interface FXClient ()

@property (nonatomic, copy, readwrite) NSString* user;
@property (nonatomic, copy, readwrite) NSString* token;
@property (readwrite, nonatomic, copy) NSString* serviceProviderIdentifier;

@end

@implementation FXClient

#pragma mark - life cycle

- (instancetype)initWithServer:(FXServer*)server {
  _server = server;
  self = [super initWithBaseURL:server.baseURL];
  if (self) {
  }
  return self;
}

+ (instancetype)sharedInstance {
  static FXClient* instance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance = [[FXClient alloc]
        initWithServer:[FXServer serverWithBaseURL:FXServerBaseUrl]];

    FXClientPassportModel* clientPassport =
        [FXClientPassportModel retrieveClientPassport];
    if (clientPassport) {
      [instance.requestSerializer
          setAuthorizationHeaderFieldWithUsername:clientPassport.clientId
                                         password:clientPassport.clientSecret];
    }

    FXClientCredentialModel* clientCredential =
        [FXClientCredentialModel retrieveClientCredential];
    if (clientCredential) {
      [instance.requestSerializer setOAuth2Token:clientCredential.accessToken
                                       tokenType:clientCredential.tokenType];
    }
  });
  return instance;
}

#pragma mark - request methods

- (RACSignal*)rac_GET:(NSString*)path
           parameters:(id)parameters
          resultClass:(Class)resultClass {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"GET"
                    resultClass:resultClass]
      setNameWithFormat:@"%@ -rac_GET: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_HEAD:(NSString*)path parameters:(id)parameters {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"HEAD"
                    resultClass:nil]
      setNameWithFormat:@"%@ -rac_HEAD: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_POST:(NSString*)path
            parameters:(id)parameters
           resultClass:(Class)resultClass {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"POST"
                    resultClass:resultClass]
      setNameWithFormat:@"%@ -rac_POST: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_POST:(NSString*)path
                   parameters:(id)parameters
    constructingBodyWithBlock:
        (void (^)(id<AFMultipartFormData> formData))block {
  return [[RACSignal createSignal:^(id<RACSubscriber> subscriber) {
    NSMutableURLRequest* request = [self.requestSerializer
        multipartFormRequestWithMethod:@"POST"
                             URLString:[[NSURL URLWithString:path
                                               relativeToURL:
                                                   self.baseURL] absoluteString]
                            parameters:parameters
             constructingBodyWithBlock:block
                                 error:nil];

    NSURLSessionDataTask* task = [self
        dataTaskWithRequest:request
          completionHandler:^(NSURLResponse* response, id responseObject,
                              NSError* error) {
            if (error) {
              NSMutableDictionary* userInfo = [error.userInfo mutableCopy];
              if (responseObject) {
                userInfo[FXResponseObjectErrorKey] = responseObject;
              }
              NSError* errorWithRes = [NSError errorWithDomain:error.domain
                                                          code:error.code
                                                      userInfo:[userInfo copy]];
              [subscriber sendError:errorWithRes];
            } else {
              [subscriber sendNext:RACTuplePack(responseObject, response)];
              [subscriber sendCompleted];
            }
          }];
    [task resume];

    return [RACDisposable disposableWithBlock:^{
      [task cancel];
    }];
  }] setNameWithFormat:
          @"%@ -rac_POST: %@, parameters: %@, constructingBodyWithBlock:",
          self.class, path, parameters];
}

- (RACSignal*)rac_PUT:(NSString*)path
           parameters:(id)parameters
          resultClass:(Class)resultClass {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"PUT"
                    resultClass:resultClass]
      setNameWithFormat:@"%@ -rac_PUT: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_PATCH:(NSString*)path
             parameters:(id)parameters
            resultClass:(Class)resultClass {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"PATCH"
                    resultClass:resultClass]
      setNameWithFormat:@"%@ -rac_PATCH: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_DELETE:(NSString*)path
              parameters:(id)parameters
             resultClass:(Class)resultClass {
  return [[self rac_requestPath:path
                     parameters:parameters
                         method:@"DELETE"
                    resultClass:resultClass]
      setNameWithFormat:@"%@ -rac_DELETE: %@, parameters: %@", self.class, path,
                        parameters];
}

- (RACSignal*)rac_requestPath:(NSString*)path
                   parameters:(id)parameters
                       method:(NSString*)method
                  resultClass:(Class)resultClass {
  RACSignal* requestSignal = [RACSignal createSignal:^(id<RACSubscriber>
                                                           subscriber) {
    NSURLRequest* request = [self.requestSerializer
        requestWithMethod:method
                URLString:[[NSURL URLWithString:path
                                  relativeToURL:self.baseURL] absoluteString]
               parameters:parameters
                    error:nil];
    @weakify(resultClass);
    NSURLSessionDataTask* task =
        [self dataTaskWithRequest:request
                completionHandler:^(NSURLResponse* response, id responseObject,
                                    NSError* error) {
                  @strongify(resultClass);
                  if (error) {
                    NSMutableDictionary* userInfo =
                        [error.userInfo mutableCopy];
                    NSInteger errorCode = error.code;
                    if (responseObject) {
                      userInfo[FXResponseObjectErrorKey] = responseObject;
                      errorCode = [responseObject[@"error_code"] integerValue];
                    }
                    NSError* errorWithRes =
                        [NSError errorWithDomain:error.domain
                                            code:errorCode
                                        userInfo:[userInfo copy]];
                    [subscriber sendError:errorWithRes];
                  } else {
                    NSError* parsingError;
                    id successObject = nil;
                    if ([responseObject isKindOfClass:NSArray.class]) {
                      successObject =
                          [MTLJSONAdapter modelsOfClass:resultClass
                                          fromJSONArray:responseObject
                                                  error:&parsingError];
                    } else {
                      successObject =
                          [MTLJSONAdapter modelOfClass:resultClass
                                    fromJSONDictionary:responseObject
                                                 error:&parsingError];
                    }

                    NSLog(@"%s====%@", __PRETTY_FUNCTION__, successObject);
                    [subscriber sendNext:RACTuplePack(successObject, response)];
                    [subscriber sendCompleted];
                  }
                }];

    [task resume];

    return [RACDisposable disposableWithBlock:^{
      [task cancel];
    }];
  }];

  //  {
  //  "type": 403,
  //  "code": 403004,
  //  "error": [
  //            "客户端不被信任",
  //            "Client is untrusted."
  //            ]
  //  },
  //  {
  //  "type": 403,
  //  "code": 403003,
  //  "error": [
  //            "客户端不可用",
  //            "Client is unavailable."
  //            ]
  //  },
  //  {
  //  "type": 403,
  //  "code": 403002,
  //  "error": [
  //            "令牌已过期",
  //            "This Token is expired."
  //            ]
  //  },
  //  {
  //  "type": 403,
  //  "code": 403001,
  //  "error": [
  //            "令牌不存在",
  //            "This Token doesn’t exist."
  //            ]
  //  },
  RACSignal* returnSignal = nil;
  if ([path containsString:FXServerOAuth2ClientSignupUrl] ||
      [path containsString:FXServerOAuth2ClientSigninUrl]) {
    returnSignal = requestSignal;
  } else {
    returnSignal = [[[self signupClientIfNeed] then:^RACSignal* {
      return [self signinClientIfNeed];
    }] then:^RACSignal* {
      return requestSignal;
    }];
  }

  return [returnSignal catch:^RACSignal*(NSError* error) {
    if (error.code > 40300) {
    }

    //    if (error.code == HBPAPIManagerErrorInvalidAccessToken) {
    //      return [[[self refreshToken] ignoreValues]
    //      concat:requestSignal];
    //    }
    return [RACSignal error:error];
  }];
}

#pragma mark - client token methods

/**
 *  @author foxsofter, 15-10-31 20:10:51
 *
 *  @brief  注册客户端
 *
 *  @return
 */
- (RACSignal*)signupClient {
  UIDevice* device = [UIDevice currentDevice];
  NSDictionary* parameters = @{
    @"type" : @"user-ios",
    @"deviceName" : device.name,
    @"deviceIdentify" : device.identifierForVendor.UUIDString,
    @"version" : @"1.0",
    @"redirectURI" : @"http://letsdo.sports",
    @"deviceVersion" : [UIDevice platformString],
    @"deviceOsVersion" : device.systemVersion,
  };

  return [self rac_POST:FXServerOAuth2ClientSignupUrl
             parameters:parameters
            resultClass:[FXClientPassportModel class]];
}

- (RACSignal*)signupClientIfNeed {
  FXClientPassportModel* clientPassport =
      [FXClientPassportModel retrieveClientPassport];
  if (!clientPassport) {
    return [[[self signupClient] doNext:^(RACTuple* tuple) {
      FXClientPassportModel* clientPassport = tuple.first;
      [FXClientPassportModel storeClientPassport:clientPassport];
      [self.requestSerializer setOAuth2Token:clientPassport.clientSecret
                                   tokenType:@"Basic"];
      NSLog(@"signupClient doNext");
    }] doError:^(NSError* error) {
      [FXClientPassportModel storeClientPassport:nil];
      NSLog(@"signupClient doError");
    }];
  }

  return [RACSignal empty];
}

/**
 *  @author foxsofter, 15-10-31 20:10:10
 *
 *  @brief  获取客户端token
 *
 *  @return
 */
- (RACSignal*)signinClient {
  return [self rac_POST:FXServerOAuth2ClientSigninUrl
             parameters:@{
               @"grant_type" : @"client_credentials",
               @"scope" : @"userclientresource"
             }
            resultClass:[FXClientCredentialModel class]];
}

- (RACSignal*)signinClientIfNeed {
  FXClientCredentialModel* clientCredential =
      [FXClientCredentialModel retrieveClientCredential];
  if (!clientCredential) {
    @weakify(self);
    return [[[self signinClient] doNext:^(RACTuple* tuple) {
      @strongify(self);
      FXClientCredentialModel* credential = tuple.first;
      [self.requestSerializer setOAuth2Token:credential.accessToken
                                   tokenType:credential.tokenType];
      [FXClientCredentialModel storeClientCredential:tuple.first];
      NSLog(@"signinClient doNext");
    }] doError:^(NSError* error) {
      @strongify(self);
      [self.requestSerializer setOAuth2Token:nil tokenType:nil];
      [FXClientCredentialModel deleteClientCredential];
      NSLog(@"signinClient doError");
    }];
  }
  return [RACSignal empty];
}

@end

@implementation AFHTTPRequestSerializer (FXOAuth2)

- (void)setOAuth2Token:(NSString*)token tokenType:(NSString*)tokenType {
  if (token) {
    [self setValue:[NSString stringWithFormat:@"%@ %@", tokenType, token]
        forHTTPHeaderField:@"Authorization"];
  } else {
    [self clearAuthorizationHeader];
  }
}

@end
