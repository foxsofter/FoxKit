//
//  FXTabBarViewModel.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXTabBarViewModelProtocol.h"

@interface FXTabBarViewModel : NSObject<FXTabBarViewModelProtocol>

// The selected view model on the array.
@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> selectedViewModel;

// The selected index on the array.
@property(nonatomic) NSUInteger selectedIndex;

// The current view model stack.
@property(nullable, nonatomic, readonly, strong)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@end
