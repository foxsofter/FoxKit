//
//  FXNavigationViewModel.m
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXNavigationViewModel.h"

@interface FXNavigationViewModel ()

@property(nullable, nonatomic, strong, readwrite)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@property(nullable, nonatomic, readwrite, strong)
    id<FXViewModelProtocol> visibleViewModel;

@end

@implementation FXNavigationViewModel

#pragma mark - life cycle

- (instancetype)initWithRootViewModel:
    (nullable id<FXViewModelProtocol>)viewModel {
  self = [super init];
  if (self) {
    _viewModels = [NSMutableArray array];
    [_viewModels addObject:viewModel];
    viewModel.navigation = self;
  }
  return self;
}

#pragma mark - navigation methods

- (void)pushViewModel:(nonnull id<FXViewModelProtocol>)viewModel
             animated:(BOOL)animated {
  [_viewModels addObject:viewModel];
  viewModel.navigation = self;
}

- (void)popViewModelAnimated:(BOOL)animated {
  [_viewModels removeLastObject];
}

- (void)popToRootViewModelAnimated:(BOOL)animated {
  [_viewModels removeAllObjects];
}

- (void)presentViewModel:(nonnull id<FXViewModelProtocol>)viewModel
                animated:(BOOL)animated
              completion:(nullable void (^)())completion {
  _visibleViewModel = viewModel;
}

- (void)dismissViewModelAnimated:(BOOL)animated
                      completion:(nullable void (^)())completion {
  _visibleViewModel = nil;
}

#pragma mark - properties

- (id<FXViewModelProtocol>)topViewModel {
  return [_viewModels lastObject];
}

- (id<FXViewModelProtocol>)visibleViewModel {
  if (_visibleViewModel) {
    return _visibleViewModel;
  }
  return [_viewModels lastObject];
}

@end
