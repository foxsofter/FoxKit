//
//  FXTabBarController.m
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXTabBarController.h"
#import "FXTabBarViewModel.h"
#import "FXViewControllerRouter.h"
#import "ReactiveCocoa.h"

@interface FXTabBarController ()

@property(nonatomic, strong, readwrite) id<FXTabBarViewModelProtocol> viewModel;

@end

@implementation FXTabBarController

#pragma mark - life cycle

+ (void)load {
  [FXViewControllerRouter
      setViewController:NSStringFromClass(FXTabBarController.class)
           forViewModel:NSStringFromClass(FXTabBarViewModel.class)];
}

- (instancetype)initWithViewModel:(id<FXTabBarViewModelProtocol>)viewModel {
  self = [super init];
  if (self) {
    NSParameterAssert(viewModel);
    self.viewModel = viewModel;

    NSMutableArray *viewControllers = [NSMutableArray array];
    for (id<FXViewModelProtocol> childViewModel in self.viewModel.viewModels) {
      UIViewController *viewController = nil;
      if (childViewModel.navigation) {
        viewController = [FXViewControllerRouter
            viewControllerForViewModel:childViewModel.navigation];
      } else {
        viewController =
            [FXViewControllerRouter viewControllerForViewModel:childViewModel];
      }
      [viewControllers addObject:viewController];
    }
    self.viewControllers = viewControllers;

    RAC(self, selectedIndex) = RACObserve(self.viewModel, selectedIndex);
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.extendedLayoutIncludesOpaqueBars = YES;
}

#pragma mark - screen style

- (BOOL)shouldAutorotate {
  return self.selectedViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
  return self.selectedViewController.supportedInterfaceOrientations;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return self.selectedViewController.preferredStatusBarStyle;
}

@end
