//
//  FXViewModel.m
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXViewModel.h"
#import "ReactiveCocoa.h"

@interface FXViewModel ()

@property(nonatomic, strong, readwrite) id<FXModelServiceProtocol> service;

@property(nullable, nonatomic, strong) RACCommand *loadingCommand;
@property (nonatomic, assign) BOOL loadingObserved;

@property(nullable, nonatomic, strong) RACCommand *loadingMoreCommand;
@property (nonatomic, assign) BOOL loadingMoreObserved;

/**
 *  @author foxsofter, 15-10-19 17:10:09
 *
 *  @brief  wait for one second
 */
@property(nonatomic, strong) RACSignal *waitSignal;

@end

@implementation FXViewModel

#pragma mark - life cycle

- (instancetype)initWithModelService:
    (nonnull id<FXModelServiceProtocol>)service {
  self = [super init];
  if (self) {
    self.service = service;
  }
  return self;
}

#pragma mark - public methods

- (RACSignal *)loadingSignal {
  return [RACSignal empty];
}

- (RACSignal *)loadingMoreSignal {
  return [RACSignal empty];
}

#pragma mark - properties

- (void)setLoading:(BOOL)loading {
  if (_loading == loading) {
    return;
  }

  NSLog(@"setLoading:%@", @(loading));
  _loading = loading;
  if (_loading) {
    [self.loadingCommand execute:nil];
  }
}

- (RACCommand *)loadingCommand {
  if (!_loadingCommand) {
    @weakify(self)
    _loadingCommand =
        [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
          @strongify(self)
          // 合并waitSignal，避免loadingSignal执行太快陷入死循环
          return [[self loadingSignal] combineLatestWith:self.waitSignal];
        }];
    // 观察命令执行是否结束，结束则设置loading＝NO
    [_loadingCommand.executing subscribeNext:^(id x) {
      if (![x boolValue]) {
        @strongify(self)
        if (!self.loadingObserved) {
          self.loadingObserved = YES;
        }else{
          self.loading = NO;
        }
      }
    }];
    // 出现错误就进行提示
    [_loadingCommand.errors subscribeNext:^(id x) {
      if (x && [x isKindOfClass:[NSError class]]) {
        @strongify(self)
        NSError *error = x;
        [self.errorSubject sendNext:error.localizedDescription];
      }
    }];
  }
  return _loadingCommand;
}

- (void)setLoadingMore:(BOOL)loadingMore {
  if (_loadingMore == loadingMore) {
    return;
  }
  _loadingMore = loadingMore;
  if (_loadingMore) {
    [self.loadingMoreCommand execute:nil];
  }
}

- (RACCommand *)loadingMoreCommand {
  if (!_loadingMoreCommand) {
    @weakify(self)
    _loadingMoreCommand =
        [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
          @strongify(self)
          // 合并waitSignal，避免loadingMoreSignal执行太快陷入死循环
          return [[self loadingMoreSignal] combineLatestWith:self.waitSignal];
        }];
    // 观察命令执行是否结束，结束则设置loadingMore＝NO
    [_loadingMoreCommand.executing subscribeNext:^(id x) {
      if (![x boolValue]) {
        @strongify(self)
        if (!self.loadingMoreObserved) {
          self.loadingMoreObserved = YES;
        }else{
          self.loadingMore = NO;
        }
      }
    }];
    // 出现错误就进行提示
    [_loadingMoreCommand.errors subscribeNext:^(id x) {
      if (x && [x isKindOfClass:[NSError class]]) {
        @strongify(self)
        NSError *error = x;
        [self.errorSubject sendNext:error.localizedDescription];
      }
    }];
  }
  return _loadingMoreCommand;
}

-(RACSubject*)successSubject {
  if (!_successSubject) {
    _successSubject = [RACSubject subject];
  }
  return _successSubject;
}

-(RACSubject*)errorSubject {
  if (!_errorSubject) {
    _errorSubject = [RACSubject subject];
  }
  return _errorSubject;
}

- (RACSignal *)waitSignal {
  return [[RACSignal empty] delay:1];
}

@end
