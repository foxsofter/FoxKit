//
//  FXModelServiceProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXClient.h"

@protocol FXModelServiceProtocol<NSObject>

@required

@property (nonatomic, strong, readonly) FXClient* client;

@end