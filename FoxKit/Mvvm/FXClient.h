//
//  FXClient.h
//  FoxKit
//  see also AFOAuth2Manager
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "ReactiveCocoa.h"

@class AFOAuthCredential;
@class FXServer;

// The domain for all errors originating in FXClient.
extern NSString* const FXClientErrorDomain;
extern NSString* const FXResponseObjectErrorKey;

typedef NS_ENUM(NSUInteger, FXAuthenticatedType) {
  FXAuthenticatedTypeNone,   // 未认证
  FXAuthenticatedTypeClient, // 客户端认证
  FXAuthenticatedTypeUser,   // 用户认证
};

@interface FXClient : AFHTTPSessionManager

/**
 *  @author foxsofter, 15-10-31 16:10:21
 *
 *  @brief  服务器信息类
 */
@property (nonatomic, strong, readonly) FXServer* server;

/**
 *  @author foxsofter, 15-10-31 16:10:51
 *
 *  @brief  判断客户端的认证类型
 */
@property (nonatomic, assign, readonly) FXAuthenticatedType authenticatedType;

/**
 *  @author foxsofter, 15-10-12 11:10:13
 *
 *  @brief  初始化网络客户端实例
 *
 *  @param server 服务器相关信息
 *
 *  @return FXClient instance
 */
- (instancetype)initWithServer:(FXServer*)server;

/**
 *  @author foxsofter, 15-10-12 11:10:30
 *
 *  @brief  客户端单例
 *
 *  @return FXClient instance
 */
+ (instancetype)sharedInstance;

/// A convenience around -GET:parameters:success:failure: that returns a cold
/// signal of the
/// resulting JSON object and response headers or error.
- (RACSignal*)rac_GET:(NSString*)path
           parameters:(id)parameters
          resultClass:(Class)resultClass;

/// A convenience around -HEAD:parameters:success:failure: that returns a cold
/// signal of the
/// resulting JSON object and response headers or error.
- (RACSignal*)rac_HEAD:(NSString*)path parameters:(id)parameters;

/// A convenience around -POST:parameters:success:failure: that returns a cold
/// signal of the
/// result.
- (RACSignal*)rac_POST:(NSString*)path
            parameters:(id)parameters
           resultClass:(Class)resultClass;

/// A convenience around
/// -POST:parameters:constructingBodyWithBlock:success:failure: that returns a
/// cold signal of the resulting JSON object and response headers or error.
- (RACSignal*)rac_POST:(NSString*)path
                   parameters:(id)parameters
    constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))block;

/// A convenience around -PUT:parameters:success:failure: that returns a cold
/// signal of the
/// resulting JSON object and response headers or error.
- (RACSignal*)rac_PUT:(NSString*)path
           parameters:(id)parameters
          resultClass:(Class)resultClass;

/// A convenience around -PATCH:parameters:success:failure: that returns a cold
/// signal of the
/// resulting JSON object and response headers or error.
- (RACSignal*)rac_PATCH:(NSString*)path
             parameters:(id)parameters
            resultClass:(Class)resultClass;

/// A convenience around -DELETE:parameters:success:failure: that returns a cold
/// signal of the
/// resulting JSON object and response headers or error.
- (RACSignal*)rac_DELETE:(NSString*)path
              parameters:(id)parameters
             resultClass:(Class)resultClass;

@end

@interface AFHTTPRequestSerializer (FXOAuth2)

- (void)setOAuth2Token:(NSString*)token tokenType:(NSString*)tokenType;

@end