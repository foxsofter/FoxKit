//
//  FXClientCredentialModel.m
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXClientCredentialModel.h"

#pragma mark -

NSString* const kFXOAuth2ClientCredentialServiceName =
    @"FXOAuth2ClientCredentialServiceName";
NSString* const kFXOAuth2ClientCredentialIdentifier =
    @"FXOAuth2ClientCredentialIdentifier";

static NSDictionary* FXKeychainQueryDictionaryWithIdentifier(
    NSString* identifier) {
  NSCParameterAssert(identifier);

  return @{
    (__bridge id)
    kSecClass : (__bridge id)kSecClassGenericPassword, (__bridge id)
    kSecAttrService : kFXOAuth2ClientCredentialServiceName, (__bridge id)
    kSecAttrAccount : identifier
  };
}

@implementation FXClientCredentialModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
  return @{
    @"accessToken" : @"access_token",
    @"tokenType" : @"token_type",
  };
}

- (NSString*)description {
  return [NSString stringWithFormat:@"<%@ accessToken:\"%@\" tokenType:\"%@\" "
                                    @"refreshToken:\"%@\">",
                                    [self class], self.accessToken,
                                    self.tokenType, self.refreshToken];
}

#pragma mark Keychain

static FXClientCredentialModel* cacheClientCredential;

+ (BOOL)storeClientCredential:(FXClientCredentialModel*)clientCredential {
  id securityAccessibility = nil;
#if (defined(__IPHONE_OS_VERSION_MAX_ALLOWED) &&                               \
     __IPHONE_OS_VERSION_MAX_ALLOWED >= 43000) ||                              \
    (defined(__MAC_OS_X_VERSION_MAX_ALLOWED) &&                                \
     __MAC_OS_X_VERSION_MAX_ALLOWED >= 1090)
  if (&kSecAttrAccessibleWhenUnlocked != NULL) {
    securityAccessibility = (__bridge id)kSecAttrAccessibleWhenUnlocked;
  }
#endif

  return [[self class] storeClientCredential:clientCredential
                           withAccessibility:securityAccessibility];
}

+ (BOOL)storeClientCredential:(FXClientCredentialModel*)clientCredential
            withAccessibility:(id)securityAccessibility {
  NSMutableDictionary* queryDictionary =
      [FXKeychainQueryDictionaryWithIdentifier(
          kFXOAuth2ClientCredentialIdentifier) mutableCopy];

  if (!clientCredential) {
    return [self deleteClientCredential];
  }

  NSMutableDictionary* updateDictionary = [NSMutableDictionary dictionary];
  updateDictionary[(__bridge id)kSecValueData] =
      [NSKeyedArchiver archivedDataWithRootObject:clientCredential];

  if (securityAccessibility) {
    updateDictionary[(__bridge id)kSecAttrAccessible] = securityAccessibility;
  }

  OSStatus status;
  BOOL exists = ([self retrieveClientCredential] != nil);

  if (exists) {
    status = SecItemUpdate((__bridge CFDictionaryRef)queryDictionary,
                           (__bridge CFDictionaryRef)updateDictionary);
  } else {
    [queryDictionary addEntriesFromDictionary:updateDictionary];
    status = SecItemAdd((__bridge CFDictionaryRef)queryDictionary, NULL);
  }

  if (status != errSecSuccess) {
    NSLog(@"Unable to %@ credential with identifier \"%@\" (Error %li)",
          exists ? @"update" : @"add", kFXOAuth2ClientCredentialIdentifier,
          (long int)status);
  }
  cacheClientCredential = clientCredential;

  return (status == errSecSuccess);
}

+ (BOOL)deleteClientCredential {
  NSMutableDictionary* queryDictionary =
      [FXKeychainQueryDictionaryWithIdentifier(
          kFXOAuth2ClientCredentialIdentifier) mutableCopy];

  OSStatus status = SecItemDelete((__bridge CFDictionaryRef)queryDictionary);

  if (status != errSecSuccess) {
    NSLog(@"Unable to delete credential with identifier \"%@\" (Error %li)",
          kFXOAuth2ClientCredentialIdentifier, (long int)status);
  }
  cacheClientCredential = nil;

  return (status == errSecSuccess);
}

+ (FXClientCredentialModel*)retrieveClientCredential {
  if (cacheClientCredential) {
    return cacheClientCredential;
  }

  NSMutableDictionary* queryDictionary =
      [FXKeychainQueryDictionaryWithIdentifier(
          kFXOAuth2ClientCredentialIdentifier) mutableCopy];
  queryDictionary[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
  queryDictionary[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;

  CFDataRef result = nil;
  OSStatus status = SecItemCopyMatching(
      (__bridge CFDictionaryRef)queryDictionary, (CFTypeRef*)&result);

  if (status != errSecSuccess) {
    NSLog(@"Unable to fetch credential with identifier \"%@\" (Error %li)",
          kFXOAuth2ClientCredentialIdentifier, (long int)status);
    return nil;
  }

  cacheClientCredential = [NSKeyedUnarchiver
      unarchiveObjectWithData:(__bridge_transfer NSData*)result];
  return cacheClientCredential;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder*)decoder {
  self = [super init];
  self.accessToken =
      [decoder decodeObjectForKey:NSStringFromSelector(@selector(accessToken))];
  self.tokenType =
      [decoder decodeObjectForKey:NSStringFromSelector(@selector(tokenType))];
  self.refreshToken = [decoder
      decodeObjectForKey:NSStringFromSelector(@selector(refreshToken))];

  return self;
}

- (void)encodeWithCoder:(NSCoder*)encoder {
  [encoder encodeObject:self.accessToken
                 forKey:NSStringFromSelector(@selector(accessToken))];
  [encoder encodeObject:self.tokenType
                 forKey:NSStringFromSelector(@selector(tokenType))];
  [encoder encodeObject:self.refreshToken
                 forKey:NSStringFromSelector(@selector(refreshToken))];
}

@end
