//
//  FXClientCredentialModel.h
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXModel.h"

@interface FXClientCredentialModel : FXModel<NSCoding>

/**
 *  @author foxsofter, 15-10-31 17:10:19
 *
 *  @brief  The OAuth access token.
 */
@property (nonatomic, copy) NSString* accessToken;

/**
 *  @author foxsofter, 15-10-31 17:10:34
 *
 *  @brief  The OAuth token type (e.g. "bearer").
 */
@property (nonatomic, copy) NSString* tokenType;

/**
 *  @author foxsofter, 15-10-31 17:10:50
 *
 *  @brief  The OAuth refresh token.
 */
@property (nonatomic, copy) NSString* refreshToken;

/**
 *  @author foxsofter, 15-10-31 22:10:52
 *
 *  @brief  "client" get token from client signin
 *          "user" get token from user signin
 */
@property (nonatomic, copy) NSString* clientType;

/**
 *  @author foxsofter, 15-10-31 22:10:56
 *
 *  @brief  save client credential to key chain
 *
 *  @param clientCredential
 *
 *  @return
 */
+ (BOOL)storeClientCredential:(FXClientCredentialModel*)clientCredential;

/**
 *  @author foxsofter, 15-10-31 22:10:23
 *
 *  @brief  get client credential from key chain
 *
 *  @return
 */
+ (FXClientCredentialModel*)retrieveClientCredential;

/**
 *  @author foxsofter, 15-10-31 22:10:40
 *
 *  @brief  delete client credential if exist
 *
 *  @return
 */
+ (BOOL)deleteClientCredential;

@end
