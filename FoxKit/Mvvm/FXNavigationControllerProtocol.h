//
//  FXNavigationControllerProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FXNavigationViewModelProtocol;

@protocol FXNavigationControllerProtocol <NSObject>

@required

- (instancetype)initWithViewModel:(nullable id<FXNavigationViewModelProtocol>)viewModel;

@property (nullable, nonatomic, strong, readonly) id<FXNavigationViewModelProtocol> viewModel;

@end