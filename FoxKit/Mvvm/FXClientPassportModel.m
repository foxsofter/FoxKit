//
//  FXClientPassportModel.m
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXClientPassportModel.h"
#import "LKUserDefaults.h"

@interface FXClientPassportDefaults : LKUserDefaults

@property (nonatomic, strong) NSString* clientId;
@property (nonatomic, strong) NSString* clientSecret;

@end

@implementation FXClientPassportDefaults

- (void)registerDefaults {
  self.clientId = @"";
  self.clientSecret = @"";
}

@end

@implementation FXClientPassportModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
  return @{
    @"clientId" : @"client_id",
    @"clientSecret" : @"client_secret",
  };
}

- (NSString*)description {
  return [NSString stringWithFormat:@"<%@ clientId:\"%@\" clientSecret:\"%@\">",
                                    [self class], self.clientId,
                                    self.clientSecret];
}

+ (void)storeClientPassport:(FXClientPassportModel*)clientPassport {
  if (clientPassport) {
    [FXClientPassportDefaults sharedInstance].clientId =
        clientPassport.clientId;
    [FXClientPassportDefaults sharedInstance].clientSecret =
        clientPassport.clientSecret;
  } else {
    [FXClientPassportDefaults sharedInstance].clientId = @"";
    [FXClientPassportDefaults sharedInstance].clientSecret = @"";
  }
}

+ (FXClientPassportModel*)retrieveClientPassport {
  FXClientPassportModel* clientPassport = [[FXClientPassportModel alloc] init];
  clientPassport.clientId = [FXClientPassportDefaults sharedInstance].clientId;
  clientPassport.clientSecret =
      [FXClientPassportDefaults sharedInstance].clientSecret;
  if (nil == clientPassport || nil == clientPassport.clientId ||
      clientPassport.clientId.length < 1 ||
      nil == clientPassport.clientSecret ||
      clientPassport.clientSecret.length < 1) {
    return nil;
  }
  return clientPassport;
}

@end
