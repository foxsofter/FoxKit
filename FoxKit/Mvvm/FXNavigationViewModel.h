//
//  FXNavigationViewModel.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXViewModelProtocol.h"
#import "FXNavigationViewModelProtocol.h"

@interface FXNavigationViewModel : NSObject<FXNavigationViewModelProtocol>

// The top view model on the stack.
@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> topViewModel;

// Return modal view model if it exists. Otherwise the top view model.
@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> visibleViewModel;

// The current view model stack.
@property(nullable, nonatomic, readonly, strong)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@end
