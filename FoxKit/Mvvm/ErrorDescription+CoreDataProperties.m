//
//  ErrorDescription+CoreDataProperties.m
//  FoxKit
//
//  Created by fox softer on 15/11/1.
//  Copyright © 2015年 foxsofter. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ErrorDescription+CoreDataProperties.h"

@implementation ErrorDescription (CoreDataProperties)

@dynamic type;
@dynamic code;
@dynamic error;

@end
