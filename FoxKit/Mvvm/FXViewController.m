//
//  FXViewController.m
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXViewController.h"
#import "FXClient.h"
#import "FXViewModelProtocol.h"
#import "FXViewModel.h"
#import "ReactiveCocoa.h"
#import "MJRefresh.h"
#import "KVNProgress.h"
#import "UIScreen+Accessor.h"


@interface FXViewController ()

@property(nonatomic, strong, readwrite) id<FXViewModelProtocol> viewModel;

@property(nonatomic, strong) MJRefreshHeader *loadingProgress;
@property(nonatomic, strong) MJRefreshFooter *loadingMoreProgress;

@end

@implementation FXViewController

#pragma mark - life cycle

+ (void)load {
  [FXViewControllerRouter
      setViewController:NSStringFromClass(FXViewController.class)
           forViewModel:NSStringFromClass(FXViewModel.class)];
}

- (instancetype)initWithViewModel:(id<FXViewModelProtocol>)viewModel {
  self = [super init];
  if (self) {
    NSParameterAssert(viewModel);

    self.viewModel = viewModel;
    
    RACChannelTo(self, title) = RACChannelTo(self.viewModel, title);

    [self subscribeActiveSignal];

    [self subscribeLoadingSignal];
    
    [self subscribeLoadingMoreSignal];
    
    [self subscribeSubmittingSignal];
    
    [self subscribeSuccessSubject];
    
    [self subscribeErrorSubject];
    
//    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
//    configuration.statusColor = [UIColor whiteColor];
//    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
//    configuration.circleStrokeForegroundColor = [UIColor whiteColor];
//    configuration.circleStrokeBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
//    configuration.circleFillBackgroundColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
//    configuration.backgroundFillColor = [UIColor colorWithRed:0.173f green:0.263f blue:0.856f alpha:0.9f];
//    configuration.backgroundTintColor = [UIColor colorWithRed:0.689 green:0.856 blue:0.370 alpha:1.000];
//    configuration.successColor = [UIColor whiteColor];
//    configuration.errorColor = [UIColor whiteColor];
//    configuration.circleSize = 110.0f;
//    configuration.lineWidth = 1.0f;
//    configuration.fullScreen = NO;
//    configuration.allowUserInteraction = NO;
    KVNProgressConfiguration *configuration = [KVNProgressConfiguration defaultConfiguration];
    configuration.minimumDisplayTime = 2;
    configuration.minimumErrorDisplayTime = 3;
    configuration.minimumSuccessDisplayTime = 2;
    
    [KVNProgress setConfiguration:configuration];
}
  return self;
}

/**
 *  @author foxsofter, 15-10-18 08:10:42
 *
 *  @brief  load default view
 */
- (void)loadView {
  UIView *rootView = [[UIView alloc] init];
  rootView.backgroundColor = [UIColor colorWithRed:0.941 green:0.945 blue:0.957 alpha:1.000];
  rootView.autoresizesSubviews = YES;
  rootView.frame = UIScreen.bounds;
  self.view = rootView;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.extendedLayoutIncludesOpaqueBars = YES;
}

#pragma mark - private methods

- (void)subscribeActiveSignal {
    // 如果视图可见，则激活viewModel
  @weakify(self)
  [[self rac_signalForSelector:@selector(viewWillAppear:)]
   subscribeNext:^(id x) {
     @strongify(self)
     self.viewModel.active = YES;
   }];
    // 视图即将隐藏，则不激活viewModel
  [[self rac_signalForSelector:@selector(viewWillDisappear:)]
   subscribeNext:^(id x) {
     @strongify(self)
     self.viewModel.active = NO;
   }];
}

- (void)subscribeLoadingSignal {
  @weakify(self)
  [[RACObserve(self, needLoading) filter:^BOOL(id value) {
    @strongify(self)
    return self.scrollView;
  }] subscribeNext:^(id x) {
    @strongify(self)
    if ([x boolValue]) {
      if (nil == _loadingProgress) {
        _loadingProgress = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
          @strongify(self)
          self.viewModel.loading = YES;
        }];
        self.scrollView.header = _loadingProgress;
      }
    } else {
      self.scrollView.header = nil;
      _loadingProgress = nil;
    }
  }];
  [[RACObserve(self.viewModel, loading) filter:^BOOL(id value) {
    @strongify(self)
    return value && self.scrollView && self.needLoading;
  }] subscribeNext:^(id x) {
    @strongify(self)
    if ([x boolValue]) {
      [self.loadingProgress beginRefreshing];
    }else{
      [self.loadingProgress endRefreshing];
    }
  }];
}

- (void)subscribeLoadingMoreSignal {
  @weakify(self)
  [[RACObserve(self, needLoadingMore) filter:^BOOL(id value) {
    @strongify(self)
    return self.scrollView;
  }] subscribeNext:^(id x) {
    @strongify(self)
    if ([x boolValue]) {
      if (nil == _loadingMoreProgress) {
        _loadingMoreProgress =
        [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
          @strongify(self)
          self.viewModel.loadingMore = YES;
        }];
        _scrollView.footer = _loadingMoreProgress;
      }
    } else {
      self.scrollView.footer = nil;
      _loadingMoreProgress = nil;
    }
  }];
  [[RACObserve(self.viewModel, loadingMore) filter:^BOOL(id value) {
    @strongify(self)
    return value && self.scrollView && self.needLoadingMore;
  }] subscribeNext:^(id x) {
    @strongify(self)
    if ([x boolValue]) {
      [self.loadingMoreProgress beginRefreshing];
    }else{
      [self.loadingMoreProgress endRefreshing];
    }
  }];
}

-(void)subscribeSubmittingSignal {
  [RACObserve(self.viewModel, submitting) subscribeNext:^(id x) {
    if ([x boolValue]) {
      [KVNProgress show];
    } else {
      [KVNProgress dismiss];
    }
  }];
}

-(void)subscribeSuccessSubject {
  [self.viewModel.successSubject subscribeNext:^(id x) {
    if (x) {
      [KVNProgress showSuccessWithStatus:x];
    }
  }];
}

-(void)subscribeErrorSubject {
  [self.viewModel.errorSubject subscribeNext:^(id x) {
    if (x) {
      [KVNProgress showErrorWithStatus:x];
    }
  }];
}


#pragma mark - properties


@end
