//
//  FXTabBarViewModelProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXViewModelProtocol.h"

@protocol FXTabBarViewModelProtocol<NSObject>

@required

- (instancetype)initWithViewModels:
    (nullable NSArray<id<FXViewModelProtocol>> *)viewModels;

@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> selectedViewModel;

@property(nonatomic) NSUInteger selectedIndex;

@property(nullable, nonatomic, readonly, strong)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@end