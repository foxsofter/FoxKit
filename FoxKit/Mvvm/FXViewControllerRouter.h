//
//  FXViewControllerRouter.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  @author foxsofter, 15-10-13 16:10:36
 *
 *  @brief  ViewController 与 ViewModel映射的类
 */
@interface FXViewControllerRouter : NSObject

/**
 *  @author foxsofter, 15-10-13 16:10:45
 *
 *  @brief  将ViewController和ViewModel匹配,请在每个ViewController
 *          的load方法里面调用此函数
 *
 *  @param viewControllerClass viewControllerClass
 *  @param viewModelClass      viewModelClass
 */
+ (void)setViewController:(NSString*)viewControllerClass
             forViewModel:(NSString*)viewModelClass;

/**
 *  @author foxsofter, 15-10-13 16:10:13
 *
 *  @brief  获取ViewController
 *
 *  @param viewModel viewModel
 *
 *  @return ViewController
 */
+ (id)viewControllerForViewModel:(id)viewModel;

@end
