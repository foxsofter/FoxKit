//
//  ErrorDescription+CoreDataProperties.h
//  FoxKit
//
//  Created by fox softer on 15/11/1.
//  Copyright © 2015年 foxsofter. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ErrorDescription.h"

NS_ASSUME_NONNULL_BEGIN

@interface ErrorDescription (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *type;
@property (nullable, nonatomic, retain) NSNumber *code;
@property (nullable, nonatomic, retain) id error;

@end

NS_ASSUME_NONNULL_END
