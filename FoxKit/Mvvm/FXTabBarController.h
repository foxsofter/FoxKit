//
//  FXTabBarController.h
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXTabBarControllerProtocol.h"

@interface FXTabBarController : UITabBarController<FXTabBarControllerProtocol>

@end
