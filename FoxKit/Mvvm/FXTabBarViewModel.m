//
//  FXTabBarViewModel.m
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXTabBarViewModel.h"

@interface FXTabBarViewModel ()

@property(nullable, nonatomic, strong, readwrite)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@end

@implementation FXTabBarViewModel

#pragma mark - life cycle

- (instancetype)initWithViewModels:
    (NSArray<id<FXViewModelProtocol>> *)viewModels {
  self = [super init];
  if (self) {
    self.viewModels = [NSMutableArray arrayWithArray:viewModels];
    for (id<FXViewModelProtocol> viewModel in viewModels) {
      viewModel.tabBar = self;
    }
    _selectedIndex = 0;
  }
  return self;
}

#pragma mark - properties

-(id<FXViewModelProtocol>)selectedViewModel {
  return [_viewModels objectAtIndex:_selectedIndex];
}

@end
