//
//  FXViewControllerRouter.m
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXViewControllerRouter.h"
#import "FXViewModelProtocol.h"
#import "FXViewControllerProtocol.h"
#import "FXNavigationViewModelProtocol.h"
#import "FXNavigationControllerProtocol.h"
#import "FXTabBarViewModelProtocol.h"
#import "FXTabBarControllerProtocol.h"

@implementation FXViewControllerRouter

static NSMutableDictionary *dictionaryOfMappings;

+ (void)setViewController:(NSString *)viewControllerClass
             forViewModel:(NSString *)viewModelClass {
  if (nil == dictionaryOfMappings) {
    dictionaryOfMappings = [NSMutableDictionary dictionary];
  }
  if ([NSClassFromString(viewModelClass)
          conformsToProtocol:@protocol(FXViewModelProtocol)]) {
    NSParameterAssert([NSClassFromString(viewControllerClass)
        conformsToProtocol:@protocol(FXViewControllerProtocol)]);
  } else if ([NSClassFromString(viewModelClass)
                 conformsToProtocol:@protocol(FXNavigationViewModelProtocol)]) {
    NSParameterAssert([NSClassFromString(viewControllerClass)
        conformsToProtocol:@protocol(FXNavigationControllerProtocol)]);
  } else if ([NSClassFromString(viewModelClass)
             conformsToProtocol:@protocol(FXTabBarViewModelProtocol)]) {
    NSParameterAssert([NSClassFromString(viewControllerClass)
                       conformsToProtocol:@protocol(FXTabBarControllerProtocol)]);
  }

  [dictionaryOfMappings setObject:viewControllerClass forKey:viewModelClass];
}

+ (id)viewControllerForViewModel:(id)viewModel {
  NSString *viewController = [dictionaryOfMappings
      valueForKey:NSStringFromClass(((NSObject *)viewModel).class)];

  NSParameterAssert([NSClassFromString(viewController)
      instancesRespondToSelector:@selector(initWithViewModel:)]);

  return
      [[NSClassFromString(viewController) alloc] initWithViewModel:viewModel];
}

@end
