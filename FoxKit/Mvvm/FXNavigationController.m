//
//  FXNavigationController.m
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXNavigationController.h"
#import "FXViewModelProtocol.h"
#import "FXNavigationViewModelProtocol.h"
#import "FXNavigationViewModel.h"
#import "FXViewController.h"
#import "ReactiveCocoa.h"

@interface FXNavigationController ()

@property(nonatomic, strong, readwrite)
    id<FXNavigationViewModelProtocol> viewModel;

@end

@implementation FXNavigationController

+ (void)load {
  [FXViewControllerRouter
      setViewController:NSStringFromClass(FXNavigationController.class)
           forViewModel:NSStringFromClass(FXNavigationViewModel.class)];
}

- (instancetype)initWithViewModel:(id<FXNavigationViewModelProtocol>)viewModel {
  self = [super init];
  if (self) {
    NSParameterAssert(viewModel);

    self.viewModel = viewModel;

    FXViewController *rootViewController =
        [FXViewControllerRouter viewControllerForViewModel:viewModel.topViewModel];
    [self setViewControllers:@[ rootViewController ] animated:NO];

    @weakify(self)
    [[self rac_signalForSelector:@selector(viewDidLoad)] subscribeNext:^(id x) {
      @strongify(self)
      [self synchronizeNavigation];
    }];
  }
  return self;
}

/**
 *  @author foxsofter, 15-10-13 15:10:44
 *
 *  @brief  同步ViewModel的导航到ViewController
 */
- (void)synchronizeNavigation {
  @weakify(self)
  [[(NSObject *)self.viewModel
      rac_signalForSelector:@selector(pushViewModel:animated:)]
      subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        UIViewController *viewController = (UIViewController *)
            [FXViewControllerRouter viewControllerForViewModel:tuple.first];
        [self pushViewController:viewController
                        animated:[tuple.second boolValue]];
      }];

  [[(NSObject *)self.viewModel
      rac_signalForSelector:@selector(popViewModelAnimated:)]
      subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        [self popViewControllerAnimated:[tuple.first boolValue]];
      }];

  [[(NSObject *)self.viewModel
      rac_signalForSelector:@selector(popToRootViewModelAnimated:)]
      subscribeNext:^(RACTuple *tuple) {
        @strongify(
            self)[self popToRootViewControllerAnimated:[tuple.first boolValue]];
      }];

  [[(NSObject *)self.viewModel
      rac_signalForSelector:@selector(presentViewModel:animated:completion:)]
      subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        id<FXViewModelProtocol> viewModel = tuple.first;
        UIViewController *viewController = nil;
        if (viewModel.navigation) {
          viewController = [FXViewControllerRouter
              viewControllerForViewModel:viewModel.navigation];
        } else {
          viewController =
              [FXViewControllerRouter viewControllerForViewModel:viewModel];
        }

        [self presentViewController:viewController
                           animated:[tuple.second boolValue]
                         completion:tuple.third];
      }];

  [[(NSObject *)self.viewModel
      rac_signalForSelector:@selector(dismissViewModelAnimated:completion:)]
      subscribeNext:^(RACTuple *tuple) {
        @strongify(self)[self.visibleViewController
            dismissViewControllerAnimated:[tuple.first boolValue]
                               completion:tuple.second];
      }];
}

- (BOOL)shouldAutorotate {
  return self.topViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
  return self.topViewController.supportedInterfaceOrientations;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return self.topViewController.preferredStatusBarStyle;
}

@end
