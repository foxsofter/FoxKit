//
//  FXViewController.h
//  FoxKit
//
//  Created by foxsofter on 15/10/11.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXViewControllerProtocol.h"
#import "FXViewControllerRouter.h"

@interface FXViewController : UIViewController<FXViewControllerProtocol>

/**
 *  @author foxsofter, 15-10-16 16:10:04
 *
 *  @brief  设置下拉刷新当前页面上的数据，生效当且仅当scrollView被有效赋值
 *          当赋值为YES，可以出现下拉刷新控件，为NO时不能出现，默认为NO
 */
@property (nonatomic, assign) BOOL needLoading;

/**
 *  @author foxsofter, 15-10-16 16:10:25
 *
 *  @brief  设置列表中上滑加载更多数据，生效当且仅当loadingView被有效赋值
 *          当赋值为YES，可以出现上拉加载更多控件，为NO时不出现，默认为NO
 */
@property (nonatomic, assign) BOOL needLoadingMore;

/**
 *  @author foxsofter, 15-10-16 16:10:15
 *
 *  @brief  需要提交表单，锁定当前视图，并出现提交进度控件
 *          当赋值为YES，可以出现提交进度控件，为NO时不可出现，默认为NO
 */
@property (nonatomic, assign) BOOL needSubmitting;

/**
 *  @author foxsofter, 15-10-17 10:10:18
 *
 *  @brief  scrollView，可以是UIScrollView， UITableView，UICollectionView，UIWebView等
 *          loadingView赋值后，可以设置needLoading和needLoadingMore
 */
@property (nullable, nonatomic, strong) __kindof UIScrollView * scrollView;

@end
