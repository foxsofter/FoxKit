//
//  FXNavigationViewModelProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FXViewModelProtocol;

@protocol FXNavigationViewModelProtocol<NSObject>

@required

- (instancetype)initWithRootViewModel:
    (nullable id<FXViewModelProtocol>)viewModel;

@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> topViewModel;

@property(nullable, nonatomic, readonly, strong)
    id<FXViewModelProtocol> visibleViewModel;

@property(nullable, nonatomic, readonly, strong)
    NSMutableArray<__kindof id<FXViewModelProtocol>> *viewModels;

@optional

- (void)pushViewModel:(nonnull id<FXViewModelProtocol>)viewModel
             animated:(BOOL)animated;

- (void)popViewModelAnimated:(BOOL)animated;

- (void)popToRootViewModelAnimated:(BOOL)animated;

- (void)presentViewModel:(nonnull id<FXViewModelProtocol>)viewModel
                animated:(BOOL)animated
              completion:(nullable void (^)())completion;

- (void)dismissViewModelAnimated:(BOOL)animated
                      completion:(nullable void (^)())completion;

@end