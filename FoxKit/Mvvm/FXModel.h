//
//  FXModel.h
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "Mantle.h"

#define CLASS_ARRAY_TRANSFORMER(propertyName, className)                       \
  +(NSValueTransformer*)propertyName##JSONTransformer {                        \
    return [NSValueTransformer                                                 \
        mtl_JSONArrayTransformerWithModelClass:[className class]];             \
  }

@interface FXModel : MTLModel<MTLJSONSerializing>

//@property(nonatomic, assign, readonly) NSUInteger statusCode;

@end
