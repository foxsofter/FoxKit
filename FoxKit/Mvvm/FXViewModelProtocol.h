//
//  FXViewModelProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RACSignal.h"

@protocol FXNavigationViewModelProtocol;
@protocol FXTabBarViewModelProtocol;
@protocol FXModelServiceProtocol;

@protocol FXViewModelProtocol<NSObject>

@required

@property (nullable, nonatomic, copy) NSString *title;

@property (nullable, nonatomic, strong) id<FXNavigationViewModelProtocol> navigation;

@property(nonnull, nonatomic, strong, readonly) id<FXModelServiceProtocol> service;

- (instancetype)initWithModelService:(nonnull id<FXModelServiceProtocol>)service;

@optional

@property (nonatomic, assign, getter = isActive) BOOL active;

@property (nullable, nonatomic, strong) id<FXTabBarViewModelProtocol> tabBar;

/**
 *  @author foxsofter, 15-10-16 16:10:04
 *
 *  @brief  下拉刷新当前页面上的数据
 */
@property (nonatomic, assign, getter=isLoading) BOOL loading;

/**
 *  @author foxsofter, 15-10-19 13:10:34
 *
 *  @brief  下拉刷新执行的函数，需要由子类实现
 *
 *  @return  pointer of loading RACSignal
 */
-(RACSignal*)loadingSignal;

/**
 *  @author foxsofter, 15-10-16 16:10:25
 *
 *  @brief  列表中上滑加载更多数据
 */
@property (nonatomic, assign, getter=isLoadingMore) BOOL loadingMore;

/**
 *  @author foxsofter, 15-10-19 13:10:03
 *
 *  @brief  上滑加载执行的函数，需要由子类实现
 *
 *  @return pointer of loadingMore RACSignal
 */
-(RACSignal*)loadingMoreSignal;

/**
 *  @author foxsofter, 15-10-16 16:10:15
 *
 *  @brief  正在提交表单
 */
@property (nonatomic, assign, getter=isSubmitting) BOOL submitting;

/**
 *  @author foxsofter, 15-10-19 23:10:07
 *
 *  @brief  成功，完成
 */
@property (nonatomic, strong) RACSubject *successSubject;

/**
 *  @author foxsofter, 15-10-19 23:10:29
 *
 *  @brief  错误
 */
@property (nonatomic, strong) RACSubject *errorSubject;

@end
