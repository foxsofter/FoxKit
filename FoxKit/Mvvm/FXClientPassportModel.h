//
//  FXClientPassportModel.h
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXModel.h"

/**
 *  @author foxsofter, 15-10-31 19:10:37
 *
 *  @brief  客户端通行证
 */
@interface FXClientPassportModel : FXModel

/**
 *  @author foxsofter, 15-10-31 16:10:09
 *
 *  @brief  client_id
 */
@property (nonatomic, copy) NSString* clientId;

/**
 *  @author foxsofter, 15-10-31 16:10:29
 *
 *  @brief  client_secret
 */
@property (nonatomic, copy) NSString* clientSecret;

/**
 *  @author foxsofter, 15-10-31 19:10:45
 *
 *  @brief  store client passport to NSUserDefaults
 *
 *  @param clientPassport
 */
+ (void)storeClientPassport:(FXClientPassportModel*)clientPassport;

/**
 *  @author foxsofter, 15-10-31 19:10:28
 *
 *  @brief  retrieve client passport from NSUserDefaults
 *
 *  @return clientPassport
 */
+ (FXClientPassportModel*)retrieveClientPassport;

@end
