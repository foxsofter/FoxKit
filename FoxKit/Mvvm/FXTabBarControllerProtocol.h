//
//  FXTabBarControllerProtocol.h
//  FoxKit
//
//  Created by foxsofter on 15/10/13.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

@protocol FXTabBarViewModelProtocol;

@protocol FXTabBarControllerProtocol <NSObject>

@required

- (instancetype)initWithViewModel:(nullable id<FXTabBarViewModelProtocol>)viewModel;

@property (nullable, nonatomic, strong, readonly) id<FXTabBarViewModelProtocol> viewModel;

@end