//
//  FXServer.h
//  FoxKit
//
//  Created by foxsofter on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkReachabilityManager.h"

@interface FXServer : NSObject

/**
 *  @author foxsofter, 15-10-10 21:10:04
 *
 *  @brief  API 域名
 */
@property (nonatomic, copy, readonly) NSURL* baseURL;

/**
 *  @author foxsofter, 15-10-10 22:10:13
 *
 *  @brief  网络状态
 */
@property (nonatomic, readonly, assign)
    AFNetworkReachabilityStatus networkReachabilityStatus;

/**
 *  @author foxsofter, 15-10-10 22:10:33
 *
 *  @brief  是否接入网络
 */
@property (readonly, nonatomic, assign, getter=isReachable) BOOL reachable;

/**
 *  @author foxsofter, 15-10-10 22:10:50
 *
 *  @brief  是否接入数据网络
 */
@property (readonly, nonatomic, assign, getter=isReachableViaWWAN)
    BOOL reachableViaWWAN;

/**
 *  @author foxsofter, 15-10-10 22:10:15
 *
 *  @brief  是否接入无线网络
 */
@property (readonly, nonatomic, assign, getter=isReachableViaWiFi)
    BOOL reachableViaWiFi;

/**
 *  @author foxsofter, 15-10-10 22:10:47
 *
 *  @brief  构造函数
 *
 *  @param baseURL 传入的接口域名
 */
+ (instancetype)serverWithBaseURL:(NSString*)baseURL;

/**
 *  @author foxsofter, 15-10-10 21:10:51
 *
 *  @brief  开始侦听服务器网络链接状态
 */
- (void)startMonitoring;

/**
 *  @author foxsofter, 15-10-10 21:10:09
 *
 *  @brief  停止侦听服务器网络链接状态
 */
- (void)stopMonitoring;

@end
