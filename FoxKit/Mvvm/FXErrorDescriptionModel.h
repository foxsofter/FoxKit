//
//  FXErrorDescriptionModel.h
//  FoxKit
//
//  Created by fox softer on 15/11/1.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXModel.h"

//  {
//  "type": 403,
//  "code": 403001,
//  "error": [
//            "令牌不存在",
//            "This Token doesn’t exist."
//            ]
//  }
@interface FXErrorDescriptionModel : FXModel

@property (nonatomic, strong) NSNumber* type;

@property (nonatomic, strong) NSNumber* code;

@property (nonatomic, strong) NSArray* error;

+ (NSString*)errorLocaleDescriptionByCode:(NSNumber*)code;

+ (NSArray<FXErrorDescriptionModel*>*)errorDescriptions;

+ (void)setErrorDescriptions:
    (NSArray<FXErrorDescriptionModel*>*)errorDescriptions;

@end
