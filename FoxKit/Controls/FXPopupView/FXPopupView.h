//
//  FXPopupView.h
//  FoxKit
//
//  Created by foxsofter on 15/10/8.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor (FXPopupView)

+(UIColor*)labelFontColor;
+(UIColor*)popupViewBackgroundColor;
+(UIColor*)primaryFontColor;
+(UIColor*)primaryBackgroundColor;
+(UIColor*)primaryTintColor;

@end

/**
 *  @author foxsofter, 15-10-08 17:10:50
 *
 *  @brief  实现从当前ViewController底部上滑的View
 */
@interface FXPopupView : UIView

/**
 *  @author foxsofter, 15-10-08 17:10:07
 *
 *  @brief  上滑显示View
 *
 *  @param contentView
 */
+ (void)show:(UIView *)contentView;

/**
 *  @author foxsofter, 15-10-08 17:10:13
 *
 *  @brief  下滑隐藏View
 */
+ (void)hide;

/**
 *  @author foxsofter, 15-10-08 17:10:04
 *
 *  @brief  需要上滑的View
 */
@property (nonatomic, strong, readonly) UIView *contentView;

/**
 *  @author foxsofter, 15-10-08 17:10:08
 *
 *  @brief  content view showing.
 */
@property (nonatomic, assign, getter=isShowing) BOOL showing;

@end
