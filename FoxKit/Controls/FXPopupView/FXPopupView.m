//
//  FXPopupView.m
//  FoxKit
//
//  Created by foxsofter on 15/10/8.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXPopupView.h"
#import "UIColor+Addition.h"
#import "UIView+Accessor.h"
#import "RACEXTScope.h"

@implementation UIColor (FXPopupView)

COLOR_IMPL_FROM_STRING(labelFont, @"#777")
COLOR_IMPL_FROM_STRING(popupViewBackground, @"FEFEFEFE")
COLOR_IMPL_FROM_STRING(primaryFont, @"#333")
COLOR_IMPL_FROM_STRING(primaryBackground, @"#333")
COLOR_IMPL_FROM_STRING(primaryTint, @"#FFDD00")

@end

@interface FXPopupView ()

@property(nonatomic, strong, readwrite) UIView *contentView;

@end

@implementation FXPopupView

static FXPopupView *_popupView;

#pragma mark - public methods

+ (void)show:(UIView *)contentView {
  if (_popupView) { // 如果已显示一个view，先清理
    [_popupView clear];
  }
  [[FXPopupView popupView] show:contentView];
}

+ (void)hide {
  [[FXPopupView popupView] hide];
}

#pragma mark - life cycle

- (void)show:(UIView *)contentView {
  _showing = YES;
  _contentView = contentView;
  [_contentView.layer setShadowColor:[UIColor blackColor].CGColor];
  [_contentView.layer setShadowOpacity:0.8];
  [_contentView.layer setShadowRadius:3.0];
  [_contentView.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
  [self addSubview:_contentView];
  _contentView.frame = CGRectMake(0, self.height - _contentView.height, self.width, _contentView.height);
  [self.superview setAlpha:1.0];

  [_contentView
      setTransform:CGAffineTransformMakeTranslation(0, _contentView.height)];
  @weakify(self)
  [UIView animateWithDuration:0.3
                   animations:^{
                     @strongify(self)
                     [self.superview setAlpha:0.8];
                     [self.contentView setAlpha:1.0];
                     [self.contentView setTransform:CGAffineTransformIdentity];
                   }
                   completion:nil];
}

- (void)hide {
  @weakify(self)
  [UIView animateWithDuration:0.3
      animations:^{
        @strongify(self)
        [self.superview setAlpha:1.0];
        [_contentView
            setTransform:CGAffineTransformTranslate(CGAffineTransformIdentity,
                                                    0, _contentView.height)];
      }
      completion:^(BOOL finished) {
        @strongify(self)
        [_contentView removeFromSuperview];
        _contentView = nil;
        [self removeFromSuperview];
        _popupView = nil;
        _showing = NO;
      }];
}

/**
 *  @author foxsofter, 15-10-09 10:10:26
 *
 *  @brief  如果背景视图被触碰，则隐藏
 *
 *  @param touches
 *  @param event
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  NSSet <UITouch *> * touchs = [event touchesForView:self];
  if (touchs.count > 0) {
    [self hide];
  }
}

#pragma mark - private methods

-(void)clear {
  [self.superview setAlpha:1.0];
  [_contentView removeFromSuperview];
  _contentView = nil;
  [self removeFromSuperview];
  _popupView = nil;
}

#pragma mark - getters & setters

+ (FXPopupView *)popupView {
  if (nil == _popupView) {
    _popupView =
        [[FXPopupView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _popupView.backgroundColor = [UIColor clearColor];
   
    NSEnumerator *frontToBackWindows =
    [[[UIApplication sharedApplication] windows] reverseObjectEnumerator];
    
    for (UIWindow *window in frontToBackWindows) {
      if (window.windowLevel == UIWindowLevelNormal && !window.hidden) {
        [window addSubview:FXPopupView.popupView];
        break;
      }
    }
  }
  
  return _popupView;
}

@end
