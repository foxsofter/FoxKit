//
//  FXPopupActionSheet.m
//  FoxKit
//
//  Created by foxsofter on 15/10/9.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXPopupActionSheet.h"
#import "FXPopupView.h"
#import "SDiPhoneVersion.h"
#import "UIScreen+Accessor.h"
#import "UIControl+Block.h"
#import "UILabel+Style.h"
#import "UIButton+Style.h"

@interface FXPopupActionSheet ()

@property (nonatomic, strong) NSDictionary *actionOptions;

@end

@implementation FXPopupActionSheet

+ (void)show:(NSString *)caption actionOptions:(NSDictionary *)actionOptions {
  CGFloat buttonHeight = 0;
  CGFloat cancelButtonHeight = 0;
  if ([SDiPhoneVersion deviceSize] <= iPhone4inch) {
    buttonHeight = 44;
    cancelButtonHeight = 54;
  } else if ([SDiPhoneVersion deviceSize] == iPhone47inch) {
    buttonHeight = 54;
    cancelButtonHeight = 64;
  } else if ([SDiPhoneVersion deviceSize] == iPhone55inch) {
    buttonHeight = 64;
    cancelButtonHeight = 74;
  }
  CGFloat width = UIScreen.width;
  CGFloat height = 0.f;
  if (caption) {
    height = (buttonHeight + 1) * (actionOptions.count + 1) + cancelButtonHeight + 1;
  }else{
    height = (buttonHeight + 1) * actionOptions.count + cancelButtonHeight + 1;
  }
  UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
  contentView.backgroundColor = [UIColor popupViewBackgroundColor];

  UIButton *cancelButton = [UIButton buttonWithCancelStyle];
  [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
  [cancelButton addTarget:self action:@selector(hideView:) forControlEvents:UIControlEventTouchUpInside];
  [contentView addSubview:cancelButton];
  cancelButton.frame = CGRectMake(0, height - cancelButtonHeight, width, cancelButtonHeight);
  
  if (caption) {
    UILabel *captionLabel = [UILabel labelWithPrimaryTipStyle];
    captionLabel.frame = CGRectMake(0, 2, width, buttonHeight - 1);
    captionLabel.text = caption;
    captionLabel.textAlignment = NSTextAlignmentCenter;
    captionLabel.textColor = [UIColor labelFontColor];
    captionLabel.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [contentView addSubview:captionLabel];
  }
  for (int i = 0; i < actionOptions.count; i++) {
    UIButton *button = [UIButton buttonWithNormalStyle];
    [contentView addSubview:button];
    if (caption) {
      button.frame = CGRectMake(0, (i + 1) * (buttonHeight + 1), width, buttonHeight);
    }else{
      button.frame = CGRectMake(0, i * (buttonHeight + 1), width, buttonHeight);
    }
    NSString *key = (actionOptions.allKeys)[i];
    [button setTitle:key forState:UIControlStateNormal];
    
    [button touchUpInside:actionOptions[key]];
    
    [button addTarget:self action:@selector(hideView:) forControlEvents:UIControlEventTouchUpInside];
  };
  
  [FXPopupView show:contentView];
}

+ (void)hideView:(UIButton *)sender {
  [FXPopupView hide];
}

+ (void)show:(NSString *)title action:(void (^)(void))action {
  [self show:nil actionOptions:@{title : action}];
}

+ (void)show:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2 {
  [self show:nil actionOptions:@{title1 : action1, title2 : action2}];
}

+ (void)show:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2
      title3:(NSString *)title3
     action3:(void (^)(void))action3 {
  [self show:nil actionOptions:@{title1 : action1, title2 : action2, title3 : action3}];
}

+ (void)show:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2
      title3:(NSString *)title3
     action3:(void (^)(void))action3
      title4:(NSString *)title4
     action4:(void (^)(void))action4 {
  [self show:nil actionOptions:@{title1 : action1, title2 : action2, title3 : action3, title4 : action4}];
}

+ (void)show:(NSString *)caption title:(NSString *)title action:(void (^)(void))action {
  [self show:caption actionOptions:@{title : action}];
}

+ (void)show:(NSString *)caption
      title1:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2 {
  [self show:caption actionOptions:@{title1 : action1, title2 : action2}];
}

+ (void)show:(NSString *)caption
      title1:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2
      title3:(NSString *)title3
     action3:(void (^)(void))action3 {
  [self show:caption actionOptions:@{title1 : action1, title2 : action2, title3 : action3}];
}

+ (void)show:(NSString *)caption
      title1:(NSString *)title1
     action1:(void (^)(void))action1
      title2:(NSString *)title2
     action2:(void (^)(void))action2
      title3:(NSString *)title3
     action3:(void (^)(void))action3
      title4:(NSString *)title4
     action4:(void (^)(void))action4 {
  [self show:caption actionOptions:@{title1 : action1, title2 : action2, title3 : action3, title4 : action4}];
}


@end
