//
//  FXBoxCollectionView.m
//  FoxKit
//
//  Created by fox softer on 15/10/26.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "FXCollectionViewBoxLayout.h"

@interface UICollectionViewLayoutAttributes (FXCollectionViewBoxLayout)

+ (instancetype)layoutAttributesForCellWithFrame:(CGRect)frame;

@end

@implementation UICollectionViewLayoutAttributes (FXCollectionViewBoxLayout)

+ (instancetype)layoutAttributesForCellWithFrame:(CGRect)frame {
  UICollectionViewLayoutAttributes *attributes =
      [UICollectionViewLayoutAttributes
          layoutAttributesForCellWithIndexPath:
              [NSIndexPath indexPathForItem:0 inSection:0]];
  if (attributes) {
    attributes.frame = frame;
  }
  return attributes;
}

@end

@interface FXCollectionViewBoxLayout ()

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign, readwrite) CGSize contentSize;

@property(nonatomic, strong) NSArray *arrayOfTemplateLayoutAttributes;

@property(nonatomic, strong) NSMutableArray *arrayOfLayoutAttributes;
@property(nonatomic, strong) NSMutableArray *arrayOfLayoutSectionContentSizes;

- (NSArray *)layoutAttributesBySection:(NSInteger)section
                numberOfItemsInSection:(NSInteger)numberOfItemsInSection;

@end

@implementation FXCollectionViewBoxLayout

#pragma mark - life cycle

- (instancetype)initWithWidth:(CGFloat)width {
  self = [super init];
  if (self) {
    self.width = width;
    [self initialize];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initialize];
  }
  return self;
}

- (void)initialize {
  CGFloat itemSpacing = 2;
  CGFloat width = self.width;
  CGFloat width_170_5 = (width - itemSpacing) / 2.f;
  CGFloat width_113 = (width - 2.f * itemSpacing) / 3.f;
  CGFloat width_228 = width - width_113 - itemSpacing;
  CGFloat width_172_5 = width_170_5 + itemSpacing;
  CGFloat width_141 = 141.f / 343.f * width;
  CGFloat width_200 = 200.f / 343.f * width;
  CGFloat width_284 = width_141 * 2.f + itemSpacing;
  self.arrayOfTemplateLayoutAttributes = @[
    @[
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width, width)]
      ]
    ],
    @[
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_170_5,
                                                        width_170_5)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_172_5, 0, width_170_5, width_170_5)]
      ]
    ],
    @[
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width,
                                                        width_200)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_200 + itemSpacing, width_170_5, width_141)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_172_5, width_200 + itemSpacing, width_170_5,
                           width_141)],
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_200,
                                                        width_284)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_200 + itemSpacing, 0, width_141, width_141)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_200 + itemSpacing,
                                                        width_141 + itemSpacing,
                                                        width_141, width_141)]
      ]
    ],
    @[
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width,
                                                        width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_228 + itemSpacing, width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_113 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)],
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_228,
                                                        width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_228 + itemSpacing, 0, width_113, width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_228 + itemSpacing, width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_113 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_228, width_113)]
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_228,
                                                        width)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_228 + itemSpacing, 0, width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_113 + itemSpacing,
                                                        width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)],
      ]
    ],
    @[
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_170_5,
                                                        width_170_5)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_172_5, 0, width_170_5, width_170_5)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, width_172_5,
                                                        width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_113 + itemSpacing,
                                                        width_172_5, width_113,
                                                        width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_172_5, width_113,
                                                        width_113)],
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_228,
                                                        width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_228 + itemSpacing, 0, width_113, width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_228 + itemSpacing, width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_113 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)]
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_228,
                                                        width_228)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_228 + itemSpacing, 0, width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_113 + itemSpacing,
                                                        width_113, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_228 + itemSpacing, width_228, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_228 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_113, width_113)]
      ],
      @[
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(0, 0, width_200,
                                                        width_170_5)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(width_200 + itemSpacing, 0, width_141, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:
                CGRectMake(0, width_172_5, width_200, width_170_5)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_200 + itemSpacing,
                                                        width_113 + itemSpacing,
                                                        width_141, width_113)],
        [UICollectionViewLayoutAttributes
            layoutAttributesForCellWithFrame:CGRectMake(width_200 + itemSpacing,
                                                        width_228 + itemSpacing,
                                                        width_141, width_113)],
      ]
    ]
  ];
}

#pragma mark - override methods

- (void)prepareLayout {
  [super prepareLayout];

  self.arrayOfLayoutAttributes = [NSMutableArray array];
  self.arrayOfLayoutSectionContentSizes = [NSMutableArray array];

  for (NSUInteger i = 0; i < [self.collectionView numberOfSections]; i++) {
    NSArray *arrayOfLayoutAttributesInSection =
        [self layoutAttributesBySection:i
                 numberOfItemsInSection:[self.collectionView
                                            numberOfItemsInSection:i]];
    if (arrayOfLayoutAttributesInSection) {
      [self.arrayOfLayoutAttributes
          addObjectsFromArray:arrayOfLayoutAttributesInSection];
      CGRect lastCellFrame =
          [[arrayOfLayoutAttributesInSection lastObject] frame];
      CGSize size =
          CGSizeMake(lastCellFrame.origin.x + lastCellFrame.size.width,
                     lastCellFrame.origin.y + lastCellFrame.size.height);
      [self.arrayOfLayoutSectionContentSizes
          addObject:[NSValue valueWithCGSize:size]];
    }
  }
}

- (CGSize)collectionViewContentSize {
  CGFloat width =
      [[self.arrayOfLayoutSectionContentSizes firstObject] CGSizeValue].width;
  CGFloat height = 0;
  for (int i = 0; i < self.arrayOfLayoutSectionContentSizes.count; i++) {
    height += [self.arrayOfLayoutSectionContentSizes[i] CGSizeValue].height;
  }
  self.contentSize = CGSizeMake(width, height);
  return self.contentSize;
}

- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)
layoutAttributesForElementsInRect:(CGRect)rect {
  NSMutableArray *elementsInRect = [NSMutableArray array];
  for (NSUInteger i = 0; i < [self.collectionView numberOfSections]; i++) {
    for (NSUInteger j = 0; j < [self.collectionView numberOfItemsInSection:i];
         j++) {
      NSIndexPath *indexPath = [NSIndexPath indexPathForItem:j inSection:i];
      UICollectionViewLayoutAttributes *layoutAttributes =
          [self layoutAttributesForItemAtIndexPath:indexPath];
      if (layoutAttributes &&
          CGRectIntersectsRect(layoutAttributes.frame, rect)) {
        [elementsInRect addObject:layoutAttributes];
      }
    }
  }
  return elementsInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:
    (NSIndexPath *)indexPath {
  for (UICollectionViewLayoutAttributes *layoutAttributes in self
           .arrayOfLayoutAttributes) {
    if ([layoutAttributes.indexPath compare:indexPath] == NSOrderedSame) {
      return layoutAttributes;
    }
  }
  return nil;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
  return !(CGSizeEqualToSize(newBounds.size, self.collectionView.frame.size));
}

#pragma mark - private methods

- (NSArray *)layoutAttributesBySection:(NSInteger)section
                numberOfItemsInSection:(NSInteger)numberOfItemsInSection {
  NSParameterAssert(numberOfItemsInSection > 0);
  NSParameterAssert(section >= 0);

  if (numberOfItemsInSection > 5) {
    numberOfItemsInSection = 5;
  }
  NSInteger randomIndex = numberOfItemsInSection < 2
                              ? 0
                              : arc4random_uniform(numberOfItemsInSection - 1);
  NSArray *arrayOfLayoutAttributes =
      [self.arrayOfTemplateLayoutAttributes[numberOfItemsInSection -
                                            1][randomIndex] copy];
  for (int i; i < numberOfItemsInSection; i++) {
    [arrayOfLayoutAttributes[i]
        setIndexPath:[NSIndexPath indexPathForItem:i inSection:section]];
  }
  return arrayOfLayoutAttributes;
}

@end
