//
//  FXBoxCollectionView.h
//  FoxKit
//
//  Created by fox softer on 15/10/26.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @author foxsofter, 15-10-26 15:10:08
 *
 *  @brief  supported no more than 5 cell in one section，
 *          display this cells in one box style.
 */
@interface FXCollectionViewBoxLayout : UICollectionViewLayout

-(instancetype)init NS_UNAVAILABLE;
-(instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

-(instancetype)initWithWidth:(CGFloat)width;

@property (nonatomic, assign, readonly) CGSize contentSize;

@end
