//
//  FXAssetsPickerController.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 12..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXAppearanceConfig.h"
#import "FXAssetsPickerController_Constant.h"
#import <CoreLocation/CoreLocation.h>

typedef NS_ENUM(NSUInteger, FXAssetsType) {
  FXAssetsTypePhoto,
  FXAssetsTypeVideo,
};

@class FXAssetsPickerController;

@protocol FXAssetsPickerControllerDelegate<NSObject>

- (void)assetsPickerController:(FXAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets;

@optional
- (void)assetsPickerControllerDidCancel:(FXAssetsPickerController *)picker;
- (void)assetsPickerControllerDidExceedMaximumNumberOfSelection:(FXAssetsPickerController *)picker;

@end

@interface FXAssetsPickerController : UIViewController

@property (nonatomic, strong) ALAssetsFilter *assetsFilter;
@property (nonatomic, strong) CLLocation * location;
@property (nonatomic, assign) NSInteger maximumNumberOfSelection;
@property (nonatomic, assign) FXAssetsType assetsType;
//--------------------------------------------------------------------
//@property (nonatomic, assign) NSInteger maximumNumberOfSelectionMedia;

@property (nonatomic, weak) id <FXAssetsPickerControllerDelegate> delegate;

+ (ALAssetsLibrary *)defaultAssetsLibrary;

/**
 *  setup the appearance, including the all the properties in FXAppearanceConfig, check FXAppearanceConfig.h out for details.
 *
 *  @param config FXAppearanceConfig instance.
 */
+ (void)setUpAppearanceConfig:(FXAppearanceConfig *)config;

@end
