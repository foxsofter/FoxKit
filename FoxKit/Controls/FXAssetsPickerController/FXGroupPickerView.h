//
//  FXGroupPickerView.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 13..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXAssetsPickerController_Constant.h"

@interface FXGroupPickerView : UIView<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UITableView *tableView;
@property (strong) NSMutableArray *groups;
@property (nonatomic,strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic,copy) intBlock blockTouchCell;
@property (nonatomic,assign) BOOL isOpen;

- (id)initWithGroups:(NSMutableArray *)groups;

- (void)show;
- (void)dismiss:(BOOL)animated;
- (void)toggle;
- (void)reloadData;

@end
