//
//  FXAppearanceConfig.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 8/26/14.
//  Copyright (c) 2015 FX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImage+FXExtension.h"

@interface FXAppearanceConfig : NSObject

@property (nonatomic, strong) NSString *assetSelectedImageName;
@property (nonatomic, strong) NSString *assetDeselectedImageName;
@property (nonatomic, strong) NSString *assetsGroupSelectedImageName;
@property (nonatomic, strong) NSString *cameraPhotoImageName;
@property (nonatomic, strong) NSString *cameraVideoImageName;
@property (nonatomic, strong) NSString *closeImageName;
@property (nonatomic, strong) UIColor *cancelSelectionTintColor;
@property (nonatomic, strong) UIColor *finishSelectionTintColor;
@property (nonatomic, assign) NSInteger assetsCountInLine;
@property (nonatomic, assign) CGFloat cellSpacing;

+ (instancetype)sharedConfig;
@end
