//
//  FXAppearanceConfig.m
//  FXAssetsPickerController
//
//  Created by foxsofter on 8/26/14.
//  Copyright (c) 2015 FX. All rights reserved.
//

#import "FXAppearanceConfig.h"

@implementation FXAppearanceConfig

+ (instancetype)sharedConfig {
  static FXAppearanceConfig *shared = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    shared = [[self alloc] init];
  });
  return shared;
}

- (instancetype)init {
  self = [super init];

  if (self) {
    self.assetsCountInLine = 3;
    self.cellSpacing = 1.0f;
    self.cancelSelectionTintColor = [UIColor colorWithWhite:0.333 alpha:1.000];
    self.finishSelectionTintColor = [UIColor colorWithWhite:0.333 alpha:1.000];
  }

  return self;
}

- (NSString *)assetSelectedImageName {
  if (!_assetSelectedImageName) {
    return @"fx_ico_photo_thumb_check";
  }
  return _assetSelectedImageName;
}

- (NSString *)assetDeselectedImageName {
  if (!_assetDeselectedImageName) {
    return @"fx_ico_photo_thumb_uncheck";
  }
  return _assetDeselectedImageName;
}

- (NSString *)assetsGroupSelectedImageName {
  if (!_assetsGroupSelectedImageName) {
    return @"fx_ico_checkMark";
  }
  return _assetsGroupSelectedImageName;
}

- (NSString *)cameraPhotoImageName {
  if (!_cameraPhotoImageName) {
    return @"fx_ico_upload_photo";
  }
  return _cameraPhotoImageName;
}

- (NSString *)cameraVideoImageName {
  if (!_cameraVideoImageName) {
    return @"fx_ico_upload_video";
  }
  return _cameraVideoImageName;
}

-(NSString *)closeImageName {
  if (!_closeImageName) {
    return @"fx_navi_icon_close";
  }
  return _closeImageName;
}

- (UIColor *)cancelSelectionTintColor {
  if (!_cancelSelectionTintColor) {
    return [UIColor grayColor];
  }
  return _cancelSelectionTintColor;
}

- (UIColor *)finishSelectionTintColor {
  if (!_finishSelectionTintColor) {
    return [UIColor grayColor];
  }
  return _finishSelectionTintColor;
}

@end
