//
//  FXGroupViewCell.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 13..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//

@import UIKit;
#import "FXAssetsPickerController_Constant.h"

@interface FXGroupViewCell : UITableViewCell

@property (nonatomic, strong) ALAssetsGroup *assetsGroup;

- (void)applyData:(ALAssetsGroup *)assetsGroup;

@end
