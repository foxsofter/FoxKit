//
//  FXWrapperPickerController.m
//  IdolChat
//
//  Created by foxsofter on 2015. 1. 28..
//  Copyright (c) 2015년 SKPlanet. All rights reserved.
//

#import "FXWrapperPickerController.h"

@interface FXWrapperPickerController ()

@end

@implementation FXWrapperPickerController

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark -  View Controller Setting /Rotation

- (BOOL)shouldAutorotate {
  return YES;
}

- (UIViewController *)childViewControllerForStatusBarHidden {
  return nil;
}

- (BOOL)prefersStatusBarHidden {
  return YES;
}

- (void)dealloc {
  //    DLog(@"FXWrapperPicerController dealloc");
}
//-(NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

@end
