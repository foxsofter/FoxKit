//
//  FXAssetsPickerController.m
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 12..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//
#import "FXAssetsPickerController.h"
#import "FXAssetsViewCell.h"
#import "FXWrapperPickerController.h"
#import "FXGroupPickerView.h"
#import <ImageIO/ImageIO.h>

@interface FXAssetsPickerController ()<
    UICollectionViewDataSource, UICollectionViewDelegate,
    UIImagePickerControllerDelegate, UINavigationControllerDelegate>
// View
@property(weak, nonatomic) IBOutlet UIImageView *imageViewTitleArrow;
@property(weak, nonatomic) IBOutlet UIButton *btnTitle;
@property(weak, nonatomic) IBOutlet UIButton *btnDone;
@property(weak, nonatomic) IBOutlet UIView *navigationTop;
@property(weak, nonatomic) IBOutlet UIView *bottomView;
@property(weak, nonatomic) IBOutlet UIButton *btnClose;
@property(weak, nonatomic) IBOutlet UILabel *selectionLabel;

@property(nonatomic, strong) UIView *noAssetView;
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) FXWrapperPickerController *picker;
@property(nonatomic, strong) FXGroupPickerView *groupPicker;

@property(nonatomic, strong) ALAssetsGroup *assetsGroup;
@property(nonatomic, strong) NSMutableArray *groups;
@property(nonatomic, strong) ALAssetsLibrary *assetsLibrary;

@property(nonatomic, strong) NSMutableArray *assets;
@property(nonatomic, assign) NSInteger numberOfSelection;

@property(nonatomic, strong) NSMutableArray *orderedSelectedItem;

- (IBAction)btnAction:(id)sender;

@end

@implementation FXAssetsPickerController

@synthesize location;

#pragma mark - ALAssetsLibrary

+ (ALAssetsLibrary *)defaultAssetsLibrary {
  static dispatch_once_t pred = 0;
  static ALAssetsLibrary *library = nil;
  dispatch_once(&pred, ^{
    library = [[ALAssetsLibrary alloc] init];
  });
  return library;
}

- (id)init {
  self = [super
      initWithNibName:@"FXAssetsPickerController"
               bundle:[NSBundle
                          bundleForClass:[FXAssetsPickerController class]]];
  if (self) {
    [[NSNotificationCenter defaultCenter]
        addObserver:self
           selector:@selector(assetsLibraryUpdated:)
               name:ALAssetsLibraryChangedNotification
             object:nil];
  }
  return self;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:ALAssetsLibraryChangedNotification
              object:nil];
  self.assetsLibrary = nil;
  self.assetsGroup = nil;
  self.assets = nil;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.
  [self initVariable];
  [self initImagePicker];

  __weak typeof(self) weakSelf = self;
  [self setupGroup:^{
    [weakSelf.groupPicker.tableView
        selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                    animated:NO
              scrollPosition:UITableViewScrollPositionNone];
  } withSetupAsset:YES];

  [self setupLayout];
  [self setupCollectionView];
  [self setupGroupPickerview];
  [self initNoAssetView];
}

- (void)initVariable {
  if (self.assetsType == FXAssetsTypePhoto) {
    self.assetsFilter = [ALAssetsFilter allPhotos];
  } else {
    self.assetsFilter = [ALAssetsFilter allVideos];
  }

  self.view.clipsToBounds = YES;
  self.orderedSelectedItem = [[NSMutableArray alloc] init];
  if (self.maximumNumberOfSelection < 1) {
    self.maximumNumberOfSelection = 1;
  }
}

- (void)initImagePicker {
  FXWrapperPickerController *picker = [[FXWrapperPickerController alloc] init];
  picker.delegate = self;
  picker.allowsEditing = NO;
  picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
  if ([UIImagePickerController
          isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    NSArray *availableMediaTypes =
        [UIImagePickerController availableMediaTypesForSourceType:
                                     UIImagePickerControllerSourceTypeCamera];
    NSMutableArray *mediaTypes =
        [NSMutableArray arrayWithArray:availableMediaTypes];

    if (self.assetsType == FXAssetsTypePhoto) {
      [mediaTypes removeObject:@"public.movie"];
    } else {
      [mediaTypes removeObject:@"public.image"];
    }

    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = mediaTypes;
  }
  self.picker = picker;
}

- (void)setupLayout {
  FXAppearanceConfig *appearanceConfig = [FXAppearanceConfig sharedConfig];
  if (self.maximumNumberOfSelection == 1) {
    self.bottomView.hidden = YES;
    self.btnDone.hidden = YES;
    [self.btnClose
        setImage:[UIImage FX_imageNamed:appearanceConfig.closeImageName]
        forState:UIControlStateNormal];
    [self.btnClose setTitle:@"" forState:UIControlStateNormal];
  } else {
    [self.btnClose setTitleColor:appearanceConfig.cancelSelectionTintColor
                        forState:UIControlStateNormal];
    [self.btnDone setTitleColor:appearanceConfig.finishSelectionTintColor
                       forState:UIControlStateNormal];
    [self.btnDone setTitleColor:[UIColor colorWithWhite:0.222 alpha:0.5]
                       forState:UIControlStateDisabled];
    UIView *lineView = [[UIView alloc]
        initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,
                                 0.5)];
    lineView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.15f];
    [self.bottomView addSubview:lineView];
  }
}

- (void)setupGroupPickerview {
  __weak typeof(self) weakSelf = self;
  self.groupPicker = [[FXGroupPickerView alloc] initWithGroups:self.groups];
  self.groupPicker.blockTouchCell = ^(NSInteger row) {
    [weakSelf changeGroup:row];
  };

  [self.view insertSubview:self.groupPicker aboveSubview:self.bottomView];
  [self.view bringSubviewToFront:self.navigationTop];
  [self menuArrowRotate];
}

- (void)setupCollectionView {
  UICollectionViewFlowLayout *layout =
      [[UICollectionViewFlowLayout alloc] init];

  FXAppearanceConfig *appearanceConfig = [FXAppearanceConfig sharedConfig];

  CGFloat itemWidth =
      ([UIScreen mainScreen].bounds.size.width -
       appearanceConfig.cellSpacing *
           ((CGFloat)appearanceConfig.assetsCountInLine - 1.0f)) /
      (CGFloat)appearanceConfig.assetsCountInLine;
  layout.itemSize = CGSizeMake(itemWidth, itemWidth);
  layout.sectionInset = UIEdgeInsetsMake(1.0, 0, 0, 0);
  layout.minimumInteritemSpacing = 1.0;
  layout.minimumLineSpacing = appearanceConfig.cellSpacing;

  CGRect rect = CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width,
                           [UIScreen mainScreen].bounds.size.height - 64 - 49);
  if (self.maximumNumberOfSelection == 1) {
    rect.size.height += 49;
  }
  self.collectionView =
      [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
  self.collectionView.allowsMultipleSelection = YES;
  [self.collectionView registerClass:[FXAssetsViewCell class]
          forCellWithReuseIdentifier:kAssetsViewCellIdentifier];

  self.collectionView.delegate = self;
  self.collectionView.dataSource = self;
  self.collectionView.backgroundColor = [UIColor whiteColor];
  self.collectionView.bounces = YES;
  self.collectionView.alwaysBounceVertical = YES;
  self.collectionView.scrollsToTop = YES;

  [self.view insertSubview:self.collectionView atIndex:0];
}

#pragma mark - public methods

+ (void)setUpAppearanceConfig:(FXAppearanceConfig *)config {
  FXAppearanceConfig *appearanceConfig = [FXAppearanceConfig sharedConfig];
  appearanceConfig.assetSelectedImageName = config.assetSelectedImageName;
  appearanceConfig.assetDeselectedImageName = config.assetDeselectedImageName;
  appearanceConfig.cameraVideoImageName = config.cameraVideoImageName;
  appearanceConfig.cameraPhotoImageName = config.cameraPhotoImageName;
  appearanceConfig.finishSelectionTintColor = config.finishSelectionTintColor;
  appearanceConfig.assetsGroupSelectedImageName =
      config.assetsGroupSelectedImageName;
  appearanceConfig.assetsCountInLine = config.assetsCountInLine;
  appearanceConfig.cellSpacing = config.cellSpacing;
}

- (void)changeGroup:(NSInteger)item {
  self.assetsGroup = self.groups[item];
  [self setupAssets:nil];
  [self.groupPicker.tableView
      selectRowAtIndexPath:[NSIndexPath indexPathForRow:item inSection:0]
                  animated:NO
            scrollPosition:UITableViewScrollPositionNone];
  [self.groupPicker dismiss:YES];
  [self.orderedSelectedItem removeAllObjects];
  [self menuArrowRotate];
}

- (void)setupGroup:(voidBlock)endblock withSetupAsset:(BOOL)doSetupAsset {
  if (!self.assetsLibrary) {
    self.assetsLibrary = [self.class defaultAssetsLibrary];
  }

  if (!self.groups)
    self.groups = [[NSMutableArray alloc] init];
  else
    [self.groups removeAllObjects];

  __weak typeof(self) weakSelf = self;

  ALAssetsFilter *assetsFilter = self.assetsFilter;

  ALAssetsLibraryGroupsEnumerationResultsBlock resultsBlock = ^(ALAssetsGroup
                                                                    *group,
                                                                BOOL *stop) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    if (group) {
      [group setAssetsFilter:assetsFilter];
      NSInteger groupType =
          [[group valueForProperty:ALAssetsGroupPropertyType] integerValue];
      if (groupType == ALAssetsGroupSavedPhotos) {
        [strongSelf.groups insertObject:group atIndex:0];
        if (doSetupAsset) {
          strongSelf.assetsGroup = group;
          [strongSelf setupAssets:nil];
        }
      } else {
        if (group.numberOfAssets > 0) [strongSelf.groups addObject:group];
      }
    }
    // traverse to the end, so reload groupPicker.
    else {
      dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.groupPicker reloadData];
        NSUInteger selectedIndex =
            [weakSelf indexOfAssetGroup:weakSelf.assetsGroup
                               inGroups:weakSelf.groups];
        if (selectedIndex != NSNotFound) {
          [weakSelf.groupPicker.tableView
              selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex
                                                      inSection:0]
                          animated:YES
                    scrollPosition:UITableViewScrollPositionNone];
        }
        if (endblock) endblock();
      });
    }
  };

  ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    [strongSelf showNotAllowed];
    strongSelf.btnDone.enabled = NO;
    [strongSelf setTitle:NSLocalizedStringFromTable(
                             @"Not Allowed", @"FXAssetsPickerController", nil)];
    [strongSelf.btnTitle setImage:nil forState:UIControlStateNormal];

  };

  [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll
                                    usingBlock:resultsBlock
                                  failureBlock:failureBlock];
}

- (void)setupAssets:(voidBlock)successBlock {
  self.title = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];

  if (!self.assets)
    self.assets = [[NSMutableArray alloc] init];
  else
    [self.assets removeAllObjects];

  if (!self.assetsGroup) {
    self.assetsGroup = self.groups[0];
  }
  [self.assetsGroup setAssetsFilter:self.assetsFilter];
  __weak typeof(self) weakSelf = self;

  ALAssetsGroupEnumerationResultsBlock resultsBlock =
      ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (asset) {
          [strongSelf.assets addObject:asset];
          strongSelf.numberOfSelection++;
        }

        else {
          dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadData];
            if (successBlock) successBlock();
          });
        }
      };
  [self.assetsGroup enumerateAssetsWithOptions:NSEnumerationReverse
                                    usingBlock:resultsBlock];
}

- (void)reloadData {
  [self.collectionView reloadData];
  self.selectionLabel.text =
      [NSString stringWithFormat:@"选定(%lu/%lu)",
                                 (unsigned long)self.collectionView
                                     .indexPathsForSelectedItems.count,
                                 self.maximumNumberOfSelection];
  //  [self showNoAssetsIfNeeded];
}

- (void)setAssetsCountWithSelectedIndexPaths:(NSArray *)indexPaths {
  self.selectionLabel.text = [NSString
      stringWithFormat:@"选定(%lu/%lu)", (unsigned long)indexPaths.count,
                       (unsigned long)self.maximumNumberOfSelection];
}

#pragma mark - Asset Exception View

- (void)initNoAssetView {
  UIView *noAssetsView =
      [[UIView alloc] initWithFrame:self.collectionView.bounds];

  CGRect rect = CGRectInset(self.collectionView.bounds, 10, 10);
  UILabel *title = [[UILabel alloc] initWithFrame:rect];
  UILabel *message = [[UILabel alloc] initWithFrame:rect];

  title.text = NSLocalizedStringFromTable(@"No Photos or Videos",
                                          @"FXAssetsPickerController", nil);
  title.font = [UIFont systemFontOfSize:19.0];
  title.textColor = [UIColor colorWithRed:153.0 / 255.0
                                    green:153.0 / 255.0
                                     blue:153.0 / 255.0
                                    alpha:1];
  title.textAlignment = NSTextAlignmentCenter;
  title.numberOfLines = 5;
  title.tag = kTagNoAssetViewTitleLabel;

  message.text = NSLocalizedStringFromTable(
      @"You can sync photos and videos onto your iPhone using iTunes.",
      @"FXAssetsPickerController", nil);
  message.font = [UIFont systemFontOfSize:15.0];
  message.textColor = [UIColor colorWithRed:153.0 / 255.0
                                      green:153.0 / 255.0
                                       blue:153.0 / 255.0
                                      alpha:1];
  message.textAlignment = NSTextAlignmentCenter;
  message.numberOfLines = 5;
  message.tag = kTagNoAssetViewMsgLabel;

  UIImageView *titleImage = [[UIImageView alloc]
      initWithImage:
          [UIImage
              imageNamed:@"FXAssetPickerController.bundle/fx_ico_no_image"]];
  titleImage.contentMode = UIViewContentModeCenter;
  titleImage.tag = kTagNoAssetViewImageView;

  [title sizeToFit];
  [message sizeToFit];

  title.center =
      CGPointMake(noAssetsView.center.x, noAssetsView.center.y - 10 -
                                             title.frame.size.height / 2 + 40);
  message.center = CGPointMake(
      noAssetsView.center.x,
      noAssetsView.center.y + 10 + message.frame.size.height / 2 + 20);
  titleImage.center =
      CGPointMake(noAssetsView.center.x, noAssetsView.center.y - 10 -
                                             titleImage.frame.size.height / 2);
  [noAssetsView addSubview:title];
  [noAssetsView addSubview:message];
  [noAssetsView addSubview:titleImage];

  [self.collectionView addSubview:noAssetsView];
  self.noAssetView = noAssetsView;
  self.noAssetView.hidden = YES;
}

- (void)showNotAllowed {
  self.title = nil;

  UIView *lockedView =
      [[UIView alloc] initWithFrame:self.collectionView.bounds];
  UIImageView *locked = [[UIImageView alloc]
      initWithImage:
          [UIImage
              imageNamed:@"FXAssetPickerController.bundle/fx_ico_no_access"]];
  locked.contentMode = UIViewContentModeCenter;

  CGRect rect = CGRectInset(self.collectionView.bounds, 8, 8);
  UILabel *title = [[UILabel alloc] initWithFrame:rect];
  UILabel *message = [[UILabel alloc] initWithFrame:rect];

  title.text = NSLocalizedStringFromTable(
      @"This app does not have access to your photos or videos.",
      @"FXAssetsPickerController", nil);
  title.font = [UIFont boldSystemFontOfSize:17.0];
  title.textColor = [UIColor colorWithRed:129.0 / 255.0
                                    green:136.0 / 255.0
                                     blue:148.0 / 255.0
                                    alpha:1];
  title.textAlignment = NSTextAlignmentCenter;
  title.numberOfLines = 5;

  message.text =
      NSLocalizedStringFromTable(@"You can enable access in Privacy Settings.",
                                 @"FXAssetsPickerController", nil);
  message.font = [UIFont systemFontOfSize:14.0];
  message.textColor = [UIColor colorWithRed:129.0 / 255.0
                                      green:136.0 / 255.0
                                       blue:148.0 / 255.0
                                      alpha:1];
  message.textAlignment = NSTextAlignmentCenter;
  message.numberOfLines = 5;

  [title sizeToFit];
  [message sizeToFit];

  locked.center =
      CGPointMake(lockedView.center.x,
                  lockedView.center.y - locked.bounds.size.height / 2 - 20);
  title.center = locked.center;
  message.center = locked.center;

  rect = title.frame;
  rect.origin.y = locked.frame.origin.y + locked.frame.size.height + 10;
  title.frame = rect;

  rect = message.frame;
  rect.origin.y = title.frame.origin.y + title.frame.size.height + 5;
  message.frame = rect;

  [lockedView addSubview:locked];
  [lockedView addSubview:title];
  [lockedView addSubview:message];
  [self.collectionView addSubview:lockedView];
}

- (void)showNoAssetsIfNeeded {
  __weak typeof(self) weakSelf = self;

  voidBlock setNoImage = ^{
    UIImageView *imgView = (UIImageView *)
        [weakSelf.noAssetView viewWithTag:kTagNoAssetViewImageView];
    imgView.contentMode = UIViewContentModeCenter;
    imgView.image =
        [UIImage imageNamed:@"FXAssetPickerController.bundle/fx_ico_no_image"];

    UILabel *title =
        (UILabel *)[weakSelf.noAssetView viewWithTag:kTagNoAssetViewTitleLabel];
    title.text = NSLocalizedStringFromTable(@"No Photos",
                                            @"FXAssetsPickerController", nil);
    UILabel *msg =
        (UILabel *)[weakSelf.noAssetView viewWithTag:kTagNoAssetViewMsgLabel];
    msg.text = NSLocalizedStringFromTable(
        @"You can sync photos onto your iPhone using iTunes.",
        @"FXAssetsPickerController", nil);
  };
  voidBlock setNoVideo = ^{
    UIImageView *imgView = (UIImageView *)
        [weakSelf.noAssetView viewWithTag:kTagNoAssetViewImageView];
    imgView.image =
        [UIImage imageNamed:@"FXAssetPickerController.bundle/fx_ico_no_video"];
    DLog(@"no video");
    UILabel *title =
        (UILabel *)[weakSelf.noAssetView viewWithTag:kTagNoAssetViewTitleLabel];
    title.text = NSLocalizedStringFromTable(@"No Videos",
                                            @"FXAssetsPickerController", nil);
    UILabel *msg =
        (UILabel *)[weakSelf.noAssetView viewWithTag:kTagNoAssetViewMsgLabel];
    msg.text = NSLocalizedStringFromTable(
        @"You can sync videos onto your iPhone using iTunes.",
        @"FXAssetsPickerController", nil);
  };

  if (self.assets.count == 0) {
    self.noAssetView.hidden = NO;
    if (self.assetsType == FXAssetsTypePhoto) {
      setNoImage();
    } else {
      setNoVideo();
    }
  } else {
    self.noAssetView.hidden = YES;
  }
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:
    (UICollectionView *)collectionView {
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return self.assets.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = kAssetsViewCellIdentifier;
  FXAssetsViewCell *cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                                forIndexPath:indexPath];
  // 单选或者第一个格子不设置选中状态
  if (indexPath.row == 0) {
    if (self.assetsType == FXAssetsTypePhoto) {
      [cell applyData:[UIImage FX_imageNamed:[FXAppearanceConfig sharedConfig]
                                                 .cameraPhotoImageName]];
    } else {
      [cell applyData:[UIImage FX_imageNamed:[FXAppearanceConfig sharedConfig]
                                                 .cameraVideoImageName]];
    }
  } else {
    [cell applyData:[self.assets objectAtIndex:indexPath.row - 1]
        singleSelection:self.maximumNumberOfSelection == 1];
  }
  return cell;
}

#pragma mark - Collection View Delegate

- (BOOL)collectionView:(UICollectionView *)collectionView
    shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row == 0) {
    if ([collectionView numberOfItemsInSection:0] > 0 &&
        [collectionView indexPathsForSelectedItems].count >=
            self.maximumNumberOfSelection) {
      return NO;
    }
    if (![UIImagePickerController
            isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
      NSString *title = NSLocalizedStringFromTable(
          @"Error", @"FXAssetsPickerController", nil);
      NSString *message = NSLocalizedStringFromTable(
          @"Device has no camera", @"FXAssetsPickerController", nil);
      UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
      [myAlertView show];
    } else {
      __weak typeof(self) weakSelf = self;
      [self presentViewController:self.picker
                         animated:YES
                       completion:^{
                         __strong typeof(self) strongSelf = weakSelf;

                         NSString *curGroupName = [[strongSelf.assetsGroup
                             valueForProperty:
                                 ALAssetsGroupPropertyURL] absoluteString];
                         NSString *cameraRollName = [[strongSelf.groups[0]
                             valueForProperty:
                                 ALAssetsGroupPropertyURL] absoluteString];

                         if (![curGroupName isEqualToString:cameraRollName]) {
                           strongSelf.assetsGroup = strongSelf.groups[0];
                           [strongSelf changeGroup:0];
                         }
                       }];
    }
    return NO;
  }
  if (self.maximumNumberOfSelection == 1) {  // 如果单选，直接允许
    return YES;
  }

  BOOL didExceedMaximumNumberOfSelection =
      [collectionView indexPathsForSelectedItems].count >=
      self.maximumNumberOfSelection;
  if (didExceedMaximumNumberOfSelection && self.delegate &&
      [self.delegate
          respondsToSelector:
              @selector(
                  assetsPickerControllerDidExceedMaximumNumberOfSelection:)]) {
    [self.delegate
        assetsPickerControllerDidExceedMaximumNumberOfSelection:self];
  }
  return !didExceedMaximumNumberOfSelection;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  ALAsset *selectedAsset = [self.assets objectAtIndex:indexPath.item - 1];
  [self.orderedSelectedItem addObject:selectedAsset];
  [self setAssetsCountWithSelectedIndexPaths:collectionView
                                                 .indexPathsForSelectedItems];
  if (self.maximumNumberOfSelection == 1) {
    [self finishPickingAssets];
  }
}

- (void)collectionView:(UICollectionView *)collectionView
    didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  ALAsset *deselectedAsset = [self.assets objectAtIndex:indexPath.item - 1];

  [self.orderedSelectedItem removeObject:deselectedAsset];
  [self setAssetsCountWithSelectedIndexPaths:collectionView
                                                 .indexPathsForSelectedItems];
}

#pragma mark - Actions

- (void)finishPickingAssets {
  NSMutableArray *assets =
      [[NSMutableArray alloc] initWithArray:self.orderedSelectedItem];

  if ([assets count] > 0) {
    FXAssetsPickerController *picker = (FXAssetsPickerController *)self;

    if ([picker.delegate respondsToSelector:@selector(assetsPickerController:
                                                      didFinishPickingAssets:)])
      [picker.delegate assetsPickerController:picker
                       didFinishPickingAssets:assets];

    [self dismissViewControllerAnimated:YES
                             completion:^{

                             }];
  }
}

#pragma mark - Helper methods

- (NSDictionary *)queryStringToDictionaryOfNSURL:(NSURL *)url {
  NSArray *urlComponents = [url.query componentsSeparatedByString:@"&"];
  if (urlComponents.count <= 0) {
    return nil;
  }
  NSMutableDictionary *queryDict = [NSMutableDictionary dictionary];
  for (NSString *keyValuePair in urlComponents) {
    NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
    [queryDict setObject:pairComponents[1] forKey:pairComponents[0]];
  }
  return [queryDict copy];
}

- (NSUInteger)indexOfAssetGroup:(ALAssetsGroup *)group
                       inGroups:(NSArray *)groups {
  NSString *targetGroupId =
      [group valueForProperty:ALAssetsGroupPropertyPersistentID];
  __block NSUInteger index = NSNotFound;
  [groups enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    ALAssetsGroup *g = obj;
    NSString *gid = [g valueForProperty:ALAssetsGroupPropertyPersistentID];
    if ([gid isEqualToString:targetGroupId]) {
      index = idx;
      *stop = YES;
    }

  }];
  return index;
}

- (NSString *)getUTCFormattedDate:(NSDate *)localDate {
  static NSDateFormatter *dateFormatter = nil;
  if (dateFormatter == nil) {
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
  }
  NSString *dateString = [dateFormatter stringFromDate:localDate];
  return dateString;
}

// Mostly from here:
// http://stackoverflow.com/questions/3884060/need-help-in-saving-geotag-info-with-photo-on-ios4-1
- (void)addGPSLocation:(NSMutableDictionary *)metaData {
  if (self.location != nil) {
    CLLocationDegrees exifLatitude = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;

    NSString *latRef;
    NSString *lngRef;
    if (exifLatitude < 0.0) {
      exifLatitude = exifLatitude * -1.0f;
      latRef = @"S";
    } else {
      latRef = @"N";
    }

    if (exifLongitude < 0.0) {
      exifLongitude = exifLongitude * -1.0f;
      lngRef = @"W";
    } else {
      lngRef = @"E";
    }

    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    if ([metaData objectForKey:(NSString *)kCGImagePropertyGPSDictionary]) {
      [locDict addEntriesFromDictionary:
                   [metaData
                       objectForKey:(NSString *)kCGImagePropertyGPSDictionary]];
    }
    [locDict setObject:[self getUTCFormattedDate:location.timestamp]
                forKey:(NSString *)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef
                forKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude]
                forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:lngRef
                forKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude]
                forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy]
                forKey:(NSString *)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude]
                forKey:(NSString *)kCGImagePropertyGPSAltitude];

    [metaData setObject:locDict
                 forKey:(NSString *)kCGImagePropertyGPSDictionary];
  }
}

#pragma mark - Notification

- (void)assetsLibraryUpdated:(NSNotification *)notification {
  // recheck here
  if (![notification.name isEqualToString:ALAssetsLibraryChangedNotification]) {
    return;
  }
  __weak typeof(self) weakSelf = self;
  dispatch_async(dispatch_get_main_queue(), ^{
    __strong typeof(self) strongSelf = weakSelf;
    NSDictionary *info = [notification userInfo];
    NSSet *updatedAssets = [info objectForKey:ALAssetLibraryUpdatedAssetsKey];
    NSSet *updatedAssetGroup =
        [info objectForKey:ALAssetLibraryUpdatedAssetGroupsKey];
    NSSet *deletedAssetGroup =
        [info objectForKey:ALAssetLibraryDeletedAssetGroupsKey];
    NSSet *insertedAssetGroup =
        [info objectForKey:ALAssetLibraryInsertedAssetGroupsKey];
    DLog(@"-------------+");
    DLog(@"updated assets:%@", updatedAssets);
    DLog(@"updated asset group:%@", updatedAssetGroup);
    DLog(@"deleted asset group:%@", deletedAssetGroup);
    DLog(@"inserted asset group:%@", insertedAssetGroup);
    DLog(@"-------------=");

    if (info == nil) {
      // AllClear
      [strongSelf setupGroup:nil withSetupAsset:YES];
      return;
    }

    if (info.count == 0) {
      return;
    }

    if (deletedAssetGroup.count > 0 || insertedAssetGroup.count > 0 ||
        updatedAssetGroup.count > 0) {
      BOOL currentAssetsGroupIsInDeletedAssetGroup = NO;
      BOOL currentAssetsGroupIsInUpdatedAssetGroup = NO;
      NSString *currentAssetGroupId = [strongSelf.assetsGroup
          valueForProperty:ALAssetsGroupPropertyPersistentID];
      // check whether user deleted a chosen assetGroup.
      for (NSURL *groupUrl in deletedAssetGroup) {
        NSDictionary *queryDictionInURL =
            [strongSelf queryStringToDictionaryOfNSURL:groupUrl];
        if ([queryDictionInURL[@"id"] isEqualToString:currentAssetGroupId]) {
          currentAssetsGroupIsInDeletedAssetGroup = YES;
          break;
        }
      }
      for (NSURL *groupUrl in updatedAssetGroup) {
        NSDictionary *queryDictionInURL =
            [strongSelf queryStringToDictionaryOfNSURL:groupUrl];
        if ([queryDictionInURL[@"id"] isEqualToString:currentAssetGroupId]) {
          currentAssetsGroupIsInUpdatedAssetGroup = YES;
          break;
        }
      }

      if (currentAssetsGroupIsInDeletedAssetGroup ||
          [strongSelf.assetsGroup numberOfAssets] == 0) {
        // if user really deletes a chosen assetGroup, make it self.groups[0] to
        // be default selected.
        [strongSelf setupGroup:^{
          [strongSelf.groupPicker reloadData];
          [strongSelf.groupPicker.tableView
              selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                          animated:NO
                    scrollPosition:UITableViewScrollPositionNone];
        } withSetupAsset:YES];
        return;
      } else {
        if (currentAssetsGroupIsInUpdatedAssetGroup) {
          NSMutableArray *selectedItems = [NSMutableArray array];
          NSArray *selectedPath =
              strongSelf.collectionView.indexPathsForSelectedItems;

          for (NSIndexPath *idxPath in selectedPath) {
            [selectedItems
                addObject:[strongSelf.assets objectAtIndex:idxPath.row - 1]];
          }
          NSInteger beforeAssets = strongSelf.assets.count;
          [strongSelf setupAssets:^{
            for (ALAsset *item in selectedItems) {
              BOOL isExist = false;
              for (ALAsset *asset in strongSelf.assets) {
                if ([[[asset valueForProperty:
                                 ALAssetPropertyAssetURL] absoluteString]
                        isEqualToString:
                            [[item
                                valueForProperty:
                                    ALAssetPropertyAssetURL] absoluteString]]) {
                  NSUInteger idx = [strongSelf.assets indexOfObject:asset];
                  NSIndexPath *newPath =
                      [NSIndexPath indexPathForRow:idx + 1 inSection:0];
                  [strongSelf.collectionView
                      selectItemAtIndexPath:newPath
                                   animated:NO
                             scrollPosition:UICollectionViewScrollPositionNone];
                  isExist = true;
                }
              }
              if (isExist == false) {
                [strongSelf.orderedSelectedItem removeObject:item];
              }
            }

            [strongSelf
                setAssetsCountWithSelectedIndexPaths:
                    strongSelf.collectionView.indexPathsForSelectedItems];
            if (strongSelf.assets.count > beforeAssets) {
              [strongSelf.collectionView setContentOffset:CGPointMake(0, 0)
                                                 animated:NO];
            }

          }];
          [strongSelf setupGroup:^{
            [strongSelf.groupPicker reloadData];
          } withSetupAsset:NO];

        } else {
          [strongSelf setupGroup:^{
            [strongSelf.groupPicker reloadData];
          } withSetupAsset:NO];
          return;
        }
      }
    }
  });
}

#pragma mark - Property

- (void)setTitle:(NSString *)title {
  [super setTitle:title];
  [self.btnTitle setTitle:title forState:UIControlStateNormal];
  [self.btnTitle setImageEdgeInsets:UIEdgeInsetsMake(5, 0, 0, 0)];
  [self.btnTitle setTitleEdgeInsets:UIEdgeInsetsMake(5, 0, 0, 0)];
  [self.btnTitle layoutIfNeeded];
}

- (void)menuArrowRotate {
  [UIView animateWithDuration:0.35
      animations:^{
        if (self.groupPicker.isOpen) {
          self.imageViewTitleArrow.transform =
              CGAffineTransformMakeRotation(M_PI);
        } else {
          self.imageViewTitleArrow.transform = CGAffineTransformIdentity;
        }
      }
      completion:^(BOOL finished){
      }];
}

#pragma mark - Control Action

- (IBAction)btnAction:(id)sender {
  UIButton *btn = (UIButton *)sender;

  switch (btn.tag) {
    case kTagButtonClose: {
      if ([self.delegate
              respondsToSelector:@selector(assetsPickerControllerDidCancel:)]) {
        [self.delegate assetsPickerControllerDidCancel:self];
      }
      [self dismissViewControllerAnimated:YES
                               completion:^{

                               }];
    } break;
    case kTagButtonFinish: {
      [self finishPickingAssets];
    } break;
    case kTagButtonGroupPicker: {
      [self.groupPicker toggle];
      [self menuArrowRotate];
    } break;
  }
}

- (void)saveAssetsAction:(NSURL *)assetURL
                   error:(NSError *)error
                 isPhoto:(BOOL)isPhoto {
  if (error) return;
  __weak typeof(self) weakSelf = self;
  [self.assetsLibrary assetForURL:assetURL
      resultBlock:^(ALAsset *asset) {
        dispatch_async(dispatch_get_main_queue(), ^{
          if (asset == nil) {
            return;
          }
          if ((self.assetsType == FXAssetsTypePhoto && isPhoto == YES) ||
              (self.assetsType == FXAssetsTypeVideo && isPhoto == NO)) {
            NSMutableArray *selectedItems = [NSMutableArray array];
            NSArray *selectedPath =
                self.collectionView.indexPathsForSelectedItems;

            for (NSIndexPath *idxPath in selectedPath) {
              [selectedItems
                  addObject:[self.assets objectAtIndex:idxPath.row - 1]];
            }

            [self.assets insertObject:asset atIndex:0];
            [self reloadData];

            for (ALAsset *item in selectedItems) {
              for (ALAsset *asset in self.assets) {
                if ([[[asset valueForProperty:
                                 ALAssetPropertyAssetURL] absoluteString]
                        isEqualToString:
                            [[item
                                valueForProperty:
                                    ALAssetPropertyAssetURL] absoluteString]]) {
                  NSUInteger idx = [self.assets indexOfObject:asset];
                  NSIndexPath *newPath =
                      [NSIndexPath indexPathForRow:idx + 1 inSection:0];
                  [self.collectionView
                      selectItemAtIndexPath:newPath
                                   animated:NO
                             scrollPosition:UICollectionViewScrollPositionNone];
                }
              }
            }
            [self.collectionView setContentOffset:CGPointMake(0, 0)
                                         animated:NO];

            if (self.maximumNumberOfSelection >
                self.collectionView.indexPathsForSelectedItems.count) {
              NSIndexPath *newPath =
                  [NSIndexPath indexPathForRow:1 inSection:0];
              [self.collectionView
                  selectItemAtIndexPath:newPath
                               animated:NO
                         scrollPosition:UICollectionViewScrollPositionNone];
              [self.orderedSelectedItem addObject:asset];
            }
            [self setAssetsCountWithSelectedIndexPaths:
                      self.collectionView.indexPathsForSelectedItems];
          }

          dispatch_after(
              dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
              dispatch_get_main_queue(), ^{
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [[NSNotificationCenter defaultCenter]
                    addObserver:strongSelf
                       selector:@selector(assetsLibraryUpdated:)
                           name:ALAssetsLibraryChangedNotification
                         object:nil];
              });
        });
      }
      failureBlock:^(NSError *err) {
        dispatch_after(
            dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
            dispatch_get_main_queue(), ^{
              __strong typeof(weakSelf) strongSelf = weakSelf;
              [[NSNotificationCenter defaultCenter]
                  addObserver:strongSelf
                     selector:@selector(assetsLibraryUpdated:)
                         name:ALAssetsLibraryChangedNotification
                       object:nil];
            });
      }];
}

#pragma mark - UIImagerPickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
  __weak typeof(self) weakSelf = self;

  if (CFStringCompare(
          (CFStringRef)[info objectForKey:UIImagePickerControllerMediaType],
          kUTTypeImage, 0) == kCFCompareEqualTo) {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [[NSNotificationCenter defaultCenter]
        removeObserver:self
                  name:ALAssetsLibraryChangedNotification
                object:nil];

    NSMutableDictionary *metaData = [NSMutableDictionary
        dictionaryWithDictionary:info[UIImagePickerControllerMediaMetadata]];
    [self addGPSLocation:metaData];

    [self.assetsLibrary
        writeImageToSavedPhotosAlbum:image.CGImage
                            metadata:metaData
                     completionBlock:^(NSURL *assetURL, NSError *error) {
                       __strong typeof(weakSelf) strongSelf = weakSelf;
                       dispatch_async(dispatch_get_main_queue(), ^{
                         [strongSelf saveAssetsAction:assetURL
                                                error:error
                                              isPhoto:YES];
                       });
                       DLog(@"writeImageToSavedPhotosAlbum");
                     }];
  } else {
    [[NSNotificationCenter defaultCenter]
        removeObserver:self
                  name:ALAssetsLibraryChangedNotification
                object:nil];
    [self.assetsLibrary
        writeVideoAtPathToSavedPhotosAlbum:info[UIImagePickerControllerMediaURL]
                           completionBlock:^(NSURL *assetURL, NSError *error) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                               [self saveAssetsAction:assetURL
                                                error:error
                                              isPhoto:NO];
                             });
                           }];
  }

  [picker dismissViewControllerAnimated:YES
                             completion:^{
                             }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [picker dismissViewControllerAnimated:YES
                             completion:^{

                             }];
}

#pragma mark - UIViewController Property

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleDefault;
}
- (UIViewController *)childViewControllerForStatusBarHidden {
  return nil;
}
- (BOOL)prefersStatusBarHidden {
  return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
  return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
  return UIInterfaceOrientationPortrait;
}

@end
