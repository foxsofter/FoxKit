//
//  FXGroupViewCell.m
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 13..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//
#import "FXGroupViewCell.h"
#import "FXAppearanceConfig.h"

@implementation FXGroupViewCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
    self.textLabel.font =
        [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:17];
    self.detailTextLabel.font =
        [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:11];
    FXAppearanceConfig *appearanceConfig = [FXAppearanceConfig sharedConfig];
    self.accessoryView = [[UIImageView alloc]
        initWithImage:[UIImage
                          FX_imageNamed:appearanceConfig
                                            .assetsGroupSelectedImageName]];
    self.selectedBackgroundView = nil;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  if (selected) {
    self.accessoryView.hidden = NO;
  } else {
    self.accessoryView.hidden = YES;
  }
}

- (void)applyData:(ALAssetsGroup *)assetsGroup {
  self.assetsGroup = assetsGroup;

  CGImageRef posterImage = assetsGroup.posterImage;
  size_t height = CGImageGetHeight(posterImage);
  float scale = height / kThumbnailLength;

  self.imageView.image = [UIImage imageWithCGImage:posterImage
                                             scale:scale
                                       orientation:UIImageOrientationUp];
  self.textLabel.text =
      [assetsGroup valueForProperty:ALAssetsGroupPropertyName];
  self.detailTextLabel.text =
      [NSString stringWithFormat:@"%ld", (long)[assetsGroup numberOfAssets]];
  self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

@end
