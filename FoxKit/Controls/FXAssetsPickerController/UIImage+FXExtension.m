//
//  UIImage+FXExtension.m
//  FXAssetsPickerController
//
//  Created by foxsofter on 8/26/14.
//  Copyright (c) 2015 FX. All rights reserved.
//

#import "UIImage+FXExtension.h"
#import "FXAssetsPickerController.h"

@implementation UIImage (FXExtension)

+ (UIImage *)FX_imageNamed:(NSString *)imageName {
  UIImage *image = [[self class] imageNamed:imageName];
  if (image) {
    return image;
  }
  NSString *imagePathInControllerBundle = [NSString
      stringWithFormat:@"FXAssetPickerController.bundle/%@", imageName];
  image = [[self class] imageNamed:imagePathInControllerBundle];
  if (image) {
    return image;
  }
  // for Swift podfile
  NSString *imagePathInBundleForClass = [NSString
      stringWithFormat:
          @"%@/FXAssetPickerController.bundle/%@",
          [[NSBundle
              bundleForClass:[FXAssetsPickerController class]] resourcePath],
          imageName];
  image = [[self class] imageNamed:imagePathInBundleForClass];
  return image;
}
@end
