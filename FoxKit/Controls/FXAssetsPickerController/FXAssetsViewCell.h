//
//  FXAssetsViewCell.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 2015. 2. 12..
//  Copyright (c) 2015 foxsofter. All rights reserved.
//

@import UIKit;

#import "FXAssetsPickerController_Constant.h"

@interface FXAssetsViewCell : UICollectionViewCell

- (void)applyData:(ALAsset *)asset singleSelection:(BOOL)singleSelection;

- (void)applyData:(UIImage *)image;

@end
