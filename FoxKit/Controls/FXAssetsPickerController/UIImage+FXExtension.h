//
//  UIImage+FXExtension.h
//  FXAssetsPickerController
//
//  Created by foxsofter on 8/26/14.
//  Copyright (c) 2015 FX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FXExtension)

/**
 *  First search main bundle, if find the image return it, otherwise search the FXAssertPickerController.bundle to get the image.
 *
 *  @param imageName name of the image.
 *
 *  @return UIImage instance or nil
 */
+ (UIImage *)FX_imageNamed:(NSString *)imageName;

@end
