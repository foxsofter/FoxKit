//
//  Conctrols.h
//  FoxKit
//
//  Created by fox softer on 15/10/14.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UILabel+Style.h"
#import "UIButton+Style.h"
#import "UITextField+Style.h"
#import "FXPopupView.h"
#import "FXPopupPickerView.h"
#import "FXPopupActionSheet.h"
#import "FXRating.h"
#import "FXSegmented.h"
#import "SphereMenu.h"
#import "SwipeView.h"
#import "KxMenu.h"
#import "FXBarButtonItem.h"
#import "FXAssetsPickerController.h"