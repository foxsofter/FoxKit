//
//  FXRating.h
//  FoxKit
//
//  Reference
//  https://github.com/TinyQ/TQStarRatingView
//  Created by foxsofter on 15/9/28.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

@import UIKit;

typedef NS_ENUM(NSUInteger, FXRatingStarStyle) {
  FXRatingStarStyleDefault, // 只能出现整个星型被选中
  FXRatingStarStyleHalf, // 只能出现半个星型
  FXRatingStarStylePrecision, // 可以出现精确值
};

@interface FXRating : UIControl

@property (nonatomic) NSUInteger maxValue;
@property (nonatomic) CGFloat minValue;
@property (nonatomic) CGFloat value;
@property (nonatomic) CGFloat spacing;
@property (nonatomic) FXRatingStarStyle starStyle;

@end
