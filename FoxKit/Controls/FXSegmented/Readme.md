<pre><code>
self.segmentedControl = [[FXSegmented alloc]
initWithSectionTitles:@[ @"Recharge", @"Withdrawal"]];
self.segmentedControl.type = FXSegmentedTypeText;
self.segmentedControl.selectedIndex = 0;
self.segmentedControl.backgroundColor = [UIColor whiteColor];
self.segmentedControl.titleTextAttributes = @{
NSForegroundColorAttributeName : [UIColor blackColor],
NSFontAttributeName : [UIFont systemFontOfSize:14.0]};
self.segmentedControl.selectedTitleTextAttributes = @{
NSForegroundColorAttributeName : [UIColor colorWithHexString:@"FAAD3E"],
NSFontAttributeName : [UIFont systemFontOfSize:14.0]};
self.segmentedControl.selectionIndicatorColor = [UIColor colorWithHexString:@"FAA736"];
self.segmentedControl.selectionStyle = FXSegmentedSelectionStyleTextWidthStripe;
self.segmentedControl.selectionIndicatorBoxOpacity = 0.1;
self.segmentedControl.selectionIndicatorLocation =
FXSegmentedSelectionIndicatorLocationBottom;
self.segmentedControl.selectionIndicatorHeight = 2;
self.segmentedControl.widthStyle = FXSegmentedWidthStyleFixed;
self.segmentedControl.borderType = FXSegmentedBorderTypeNone;
self.segmentedControl.borderColor = [UIColor colorWithHexString:@"F8BF84"];
__weak typeof(self) weakSelf = self;
[self.segmentedControl setSelectionChanged:^(NSInteger index) {
switch (index) {
case 0:
break;
case 1: {
} break;
default:
break;
}
}];
</code></pre>