//
//  FXSegmented.h
//  DDPanGu
//
//  Created by foxsofter on 15/2/13.
//  Copyright (c) 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FXSegmented;

typedef void (^SelectionChangedBlock)(NSInteger index);

typedef NSAttributedString * (^TitleFormatterBlock)(
    FXSegmented *segmentedControl, NSString *title, NSUInteger index,
    BOOL selected);

typedef NS_ENUM(NSInteger, FXSegmentedSelectionStyle) {
  FXSegmentedSelectionStyleTextWidthStripe,
  FXSegmentedSelectionStyleFullWidthStripe,
  FXSegmentedSelectionStyleBox,
  FXSegmentedSelectionStyleArrow
};

typedef NS_ENUM(NSInteger, FXSegmentedSelectionIndicatorLocation) {
  FXSegmentedSelectionIndicatorLocationTop,
  FXSegmentedSelectionIndicatorLocationBottom,
  FXSegmentedSelectionIndicatorLocationNone
};

typedef NS_ENUM(NSInteger, FXSegmentedWidthStyle) {
  FXSegmentedWidthStyleFixed,
  FXSegmentedWidthStyleDynamic,
};

typedef NS_OPTIONS(NSInteger, FXSegmentedBorderType) {
  FXSegmentedBorderTypeNone = 0,
  FXSegmentedBorderTypeTop = (1 << 0),
  FXSegmentedBorderTypeLeft = (1 << 1),
  FXSegmentedBorderTypeBottom = (1 << 2),
  FXSegmentedBorderTypeRight = (1 << 3)
};

enum { FXSegmentedNoSegment = -1 };

typedef NS_ENUM(NSInteger, FXSegmentedType) {
  FXSegmentedTypeText,
  FXSegmentedTypeImages,
  FXSegmentedTypeTextImages
};

@interface FXScrollView : UIScrollView
@end

@interface FXSegmented : UIControl

@property(nonatomic, strong) NSArray *sectionTitles;
@property(nonatomic, strong) NSArray *sectionImages;
@property(nonatomic, strong) NSArray *sectionSelectedImages;

@property(nonatomic, copy) SelectionChangedBlock selectionChanged;
@property(nonatomic, copy) TitleFormatterBlock titleFormatter;
@property(nonatomic, strong)
    NSDictionary *titleTextAttributes UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)
    NSDictionary *selectedTitleTextAttributes UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong) UIColor *backgroundColor UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)
    UIColor *selectionIndicatorColor UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)
    UIColor *verticalDividerColor UI_APPEARANCE_SELECTOR;
@property(nonatomic, assign) CGFloat selectionIndicatorBoxOpacity;
@property(nonatomic, assign) CGFloat verticalDividerWidth;
@property(nonatomic, assign) FXSegmentedType type;
@property(nonatomic, assign) FXSegmentedSelectionStyle selectionStyle;
@property(nonatomic, assign) FXSegmentedWidthStyle widthStyle;
@property(nonatomic, assign)
    FXSegmentedSelectionIndicatorLocation selectionIndicatorLocation;
@property(nonatomic, assign) FXSegmentedBorderType borderType;
@property(nonatomic, strong) UIColor *borderColor;
@property(nonatomic, assign) CGFloat borderWidth;
@property(nonatomic, getter=isUserDraggable) BOOL userDraggable;
@property(nonatomic, getter=isTouchEnabled) BOOL touchEnabled;
@property(nonatomic, getter=isVerticalDividerEnabled)
    BOOL verticalDividerEnabled;
@property(nonatomic, assign) NSInteger selectedIndex;
@property(nonatomic, readwrite) CGFloat selectionIndicatorHeight;
@property(nonatomic, readwrite) UIEdgeInsets selectionIndicatorEdgeInsets;
@property(nonatomic, readwrite) UIEdgeInsets segmentEdgeInset;
@property(nonatomic) BOOL shouldAnimateUserSelection;

- (instancetype)initWithSectionTitles:(NSArray *)sectionTitles;
- (instancetype)initWithSectionImages:(NSArray *)sectionImages
                sectionSelectedImages:(NSArray *)sectionSelectedImages
    NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithSectionImages:(NSArray *)sectionImages
                sectionSelectedImages:(NSArray *)sectionSelectedImages
                    titlesForSections:(NSArray *)sectionTitles
    NS_DESIGNATED_INITIALIZER;
- (void)setSelectedIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)setSelectedIndex:(NSUInteger)index
                animated:(BOOL)animated
                  notify:(BOOL)notify;

- (void)setSelectionChanged:(SelectionChangedBlock)selectionChanged;

/**
 *  @author foxsofter, 15-10-24 12:10:04
 *
 *  @brief  获取空间实际的宽度，方便将控件居中
 */
@property (nonatomic, assign, readonly) CGFloat actualWidth;

@end
