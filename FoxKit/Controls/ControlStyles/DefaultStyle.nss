@primaryFontName: system;
@primaryFontNameBold: boldSystem;
@secondaryFontName: system;
@secondaryFontNameBold: boldSystem;

@primaryFontColor: #333;
@secondaryFontColor: #777;
@primaryTipFontColor: #999;
@secondaryTipFontColor: #C0C0C0;

@primaryBackgroundColor: #333;
@primaryTintColor: #FFDD00;
@secondaryTintColor: #FF9900;

@linkButtonTextColor:#03BBCF;
@navFontColor:#555;


BarButton {
  font-name: @primaryFontName;
  font-color: @navFontColor;
  text-shadow-color: clear;
}

Button {
  background-color: #FFFFFF;
  font-name: @primaryFontName;
  font-color: @primaryFontColor;
  font-size: 18;
  height: 37;
  exclude-views: UIAlertButton;
  exclude-subviews: UITableViewCell,UITextField;
}
SubmitButton {
  background-color: #FFD800;
  background-color-disabled: #FFD800
  background-color-highlighted: #FFDF33
  font-color: rgba(51,51,51,1);
  font-color-disabled: rgba(51,51,51,0.5);
  font-color-highlighted: rgba(51,51,51,0.7);
  font-size: 18;
  corner-radius: 4;
}
CancelButton{
  background-color: #EFEFEF;
}

Label {
  font-size: 18;
  font-name: @primaryFontName;
  font-color: @primaryFontColor;
  text-auto-fit: false;
}
PrimaryLabel {
  font-name: @primaryFontName;
  font-color: @primaryFontColor;
}
SecondaryLabel {
  font-name: @secondaryFontName;
  font-color: @secondaryFontColor;
}
PrimaryTipLabel {
  font-name: @primaryTipFontName;
  font-color: @primaryTipFontColor;
}
SecondaryTipLabel {
  font-name: @secondaryTipFontName;
  font-color: @secondaryTipFontColor;
}


NavigationBar {
  font-name: @primaryFontName;
  font-size: 20;
  font-color: @navFontColor;
  text-shadow-color: clear;
  background-color-top: #54B4EB;
  background-color-bottom: #2FA4E7;
}
/*SearchBar {*/
/*  background-color-top: @primaryBackgroundColorTop;*/
/*  background-color-bottom: @primaryBackgroundColorBottom;*/
/*  scope-background-color: #FFFFFF;*/
/*}*/
/*SegmentedControl {*/
/*background-color: @primaryBackgroundColorTop;*/
/*background-color-selected: #EEEEEE;*/
/*border-color: #999999;*/
/*border-width: @primaryBorderWidth;*/
/*corner-radius: 7;*/
/*font-name: @secondaryFontNameBold;*/
/*font-size: 13;*/
/*font-color: @primaryFontColor;*/
/*text-shadow-color: clear;*/
/*}*/
/*Switch {*/
/*on-tint-color: @primaryBackgroundTintColor;*/
/*}*/
TabBar {
  background-color: red;
  background-tint-color: red;
}
TabBarItem {
  font-name: @primaryFontName;
  font-color: #FFF;
  font-color-selected: @primaryTintColor;
  font-size: 21;
  /*  background-image-selected (Image)*/
  /*  background-image-selected-insets (Box)*/
  /*  finished-image (Image)*/
  /*  finished-image-selected (Image)*/
}
/*TableCell {*/
/*background-color-top: @secondaryBackgroundColorTop;*/
/*background-color-bottom: @secondaryBackgroundColorBottom;*/
/*font-color: @primaryFontColor;*/
/*font-name: @secondaryFontNameBold;*/
/*font-size: 17;*/
/*}*/
/*TableCellDetail {*/
/*font-name: @secondaryFontName;*/
/*font-size: 14;*/
/*font-color: @secondaryFontColor;*/
/*}*/
/*TextField {*/
/*height: 37;*/
/*font-name: @inputFontName;*/
/*font-color: @primaryFontColor;*/
/*font-size: 18;*/
/*padding: 0 10 0 10;*/
/*border-style: rounded;*/
/*border-width: @primaryBorderWidth;*/
/*border-color: @primaryBorderColor;*/
/*corner-radius: 4;*/
/*vertical-align: center;*/
/*}*/
/*LargeTextField {*/
/*height: 50;*/
/*}*/
/*LargeTextField {*/
/*font-size: 28;*/
/*}*/
View {
}
