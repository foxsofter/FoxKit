//
//  UITextField+Style.h
//  FoxKit
//
//  Created by fox softer on 15/9/25.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Style)

+(instancetype)textFieldWithNormalStyle;

@end
