//
//  UITextField+Style.m
//  FoxKit
//
//  Created by fox softer on 15/9/25.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UITextField+Style.h"
#import "UITextField+NUI.h"

@implementation UITextField (Style)

+(instancetype)textFieldWithNormalStyle {
  UITextField *textField = [UITextField new];
//  textField.nuiClass = @"LargeTextField";
  [textField applyNUI];
  textField.tintColor = [UIColor blackColor];
  return textField;
}

@end
