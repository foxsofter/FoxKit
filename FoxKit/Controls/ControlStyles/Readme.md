https://github.com/tombenner/nui

BarButton

UIBarButtonItem

background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-color-highlighted (Color)
background-image (Image)
background-image-insets (Box)
background-tint-color (Color)
border-color (Color)
border-width (Number)
corner-radius (Number)
font-color (Color)
font-name (FontName)
font-size (Number)
text-shadow-color (Color)
text-shadow-offset (Offset)

BarButtonBack

UIBarButtonItem back button, inherits from BarButton

background-color (Color)
background-image (Image)
background-image-insets (Box)
background-tint-color (Color)
border-color (Color)
border-width (Number)
corner-radius (Number)
font-color (Color)
font-name (FontName)
font-size (Number)
text-shadow-color (Color)
text-shadow-offset (Offset)
Button

UIButton

background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-color-disabled (Color)
background-color-highlighted (Color)
background-color-selected (Color)
background-color-selected-highlighted (Color)
background-image (Image)
background-image-insets (Box)
background-image-disabled (Image)
background-image-disabled-insets (Box)
background-image-highlighted (Image)
background-image-highlighted-insets (Box)
background-image-selected (Image)
background-image-selected-insets (Box)
background-image-selected-disabled (Image)
background-image-selected-disabled-insets (Box)
background-image-selected-highlighted (Image)
background-image-selected-highlighted-insets (Box)
border-color (Color)
border-width (Number)
content-insets (Box)
corner-radius (Number)
font-color (Color)
font-color-disabled (Color)
font-color-highlighted (Color)
font-color-selected (Color)
font-color-selected-disabled (Color)
font-color-selected-highlighted (Color)
font-name (FontName)
font-size (Number)
height (Number)
image (Image)
image-insets (Box)
image-disabled (Image)
image-disabled-insets (Box)
image-highlighted (Image)
image-highlighted-insets (Box)
image-selected (Image)
image-selected-insets (Box)
image-selected-disabled (Image)
image-selected-disabled-insets (Box)
image-selected-highlighted (Image)
image-selected-highlighted-insets (Box)
padding (Box)
shadow-color (Color)
shadow-offset (Offset)
shadow-opacity (Number)
shadow-radius (Number)
text-align (TextAlign)
text-alpha (Number)
text-auto-fit (Boolean)
text-shadow-color (Color)
text-shadow-color-disabled (Color)
text-shadow-color-highlighted (Color)
text-shadow-color-selected (Color)
text-shadow-color-selected-disabled (Color)
text-shadow-color-selected-highlighted (Color)
text-shadow-offset (Offset)
text-transform (TextTransform)
title-insets (Box)
width (Number)
Control

UIControl

background-color (Color)
background-image (Image)
border-color (Color)
border-width (Number)
corner-radius (Number)
shadow-color (Color)
shadow-offset (Offset)
shadow-opacity (Number)
shadow-radius (Number)
Label

UILabel

background-color (Color)
border-color (Color)
border-width (Number)
corner-radius (Number)
font-color (Color)
font-color-highlighted (Color)
font-name (FontName)
font-size (Number)
height (Number)
shadow-color (Color)
shadow-offset (Offset)
shadow-opacity (Number)
shadow-radius (Number)
text-align (TextAlign)
text-alpha (Number)
text-auto-fit (Boolean)
text-shadow-color (Color)
text-shadow-offset (Offset)
text-transform (TextTransform)
width (Number)
NavigationBar

UINavigationBar

bar-tint-color (Color)
background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-image (Image)
background-image-insets (Box)
background-tint-color (Color)
font-color (Color)
font-name (FontName)
font-size (Number)
shadow-image (Image)
text-shadow-color (Color)
text-shadow-offset (Offset)
title-vertical-offset (Number)
Progress

UIProgressView

progress-image (Image)
progress-tint-color (Color)
track-image (Image)
track-tint-color (Color)
width (Number)
SearchBar

UISearchBar

background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-image (Image)
background-tint-color (Color)
scope-background-color (Color)
scope-background-image (Image)
SearchBarButton

UISearchBar button, inherits from BarButton

See Button

SearchBarScopeBar

UISearchBar scope bar, inherits from SegmentedControl

See SegmentedControl

SegmentedControl

UISegmentedControl

background-color (Color)
background-color-selected (Color)
background-image (Image)
background-image-insets (Box)
background-image-selected (Image)
background-image-selected-insets (Box)
background-tint-color (Color)
border-color (Color)
border-width (Number)
corner-radius (Number)
divider-color (Color)
divider-image (Image)
font-color (Color)
font-color-selected (Color)
font-name (FontName)
font-size (Number)
text-shadow-color (Color)
text-shadow-color-selected (Color)
text-shadow-offset (Offset)
text-shadow-offset-selected (Offset)
Slider

UISlider

minimum-track-tint-color (Color)
maximum-track-tint-color (Color)
minimum-value-image (Image)
maximum-value-image (Image)
thumb-image (Image)
thumb-tint-color (Color)
Switch

UISwitch

background-color (Color)
off-image (Image)
off-image-insets (Box)
on-image (Image)
on-image-insets (Box)
on-tint-color (Color)
thumb-tint-color (Color)
tint-color (Color)
TabBar

UITabBar

background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-image (Image)
background-image-insets (Box)
background-tint-color (Color)
selected-image (Image)
selected-image-tint-color (Color)

TabBarItem

UITabBarItem

background-image-selected (Image)
background-image-selected-insets (Box)
finished-image (Image)
finished-image-selected (Image)
font-color (Color)
font-color-selected (Color)
font-name (FontName)
font-size (Number)
text-offset (Offset)
text-shadow-color (Color)
text-shadow-offset (Offset)
Table

UITableView

background-color (Color)
background-color-top/background-color-bottom (Gradient)
row-height (Number)
separator-color (Color)
separator-style (SeparatorStyle)
TableCell

UITableViewCell

background-color (Color)
background-color-top/background-color-bottom (Gradient)
background-color-selected (Color)
background-color-top-selected/background-color-bottom-selected (Gradient)
font-color (Color)
font-color-highlighted (Color)
font-name (FontName)
font-size (Number)
text-align (TextAlign)
text-alpha (Number)
text-auto-fit (Boolean)
text-shadow-color (Color)
text-shadow-offset (Offset)
TableCellDetail

The detail label of a UITableViewCell

font-color (Color)
font-color-highlighted (Color)
font-name (FontName)
font-size (Number)
text-align (TextAlign)
text-alpha (Number)
text-auto-fit (Boolean)
text-shadow-color (Color)
text-shadow-offset (Offset)
Toolbar

UIToolbar

background-color (Color)
background-image-top (Image)
background-image-bottom (Image)
background-image-top-landscape (Image)
background-image-bottom-landscape (Image)
background-tint-color (Color)
shadow-image (Image)
shadow-image-top (Image)
shadow-image-bottom (Image)
TextField

UITextField

background-color (Color)
background-image (Image)
background-image-insets (Box)
border-color (Color)
border-style (BorderStyle)
border-width (Number)
corner-radius (Number)
font-color (Color)
font-name (FontName)
font-size (Number)
height (Number)
padding (Box)
shadow-color (Color)
shadow-offset (Offset)
shadow-opacity (Number)
shadow-radius (Number)
vertical-align (VerticalAlign)
width (Number)

TextView

UITextView

font-color (Color)
font-name (FontName)
font-size (Number)
padding (Box)
View

UIView

background-color (Color)
background-image (Image)
border-color (Color)
border-width (Number)
corner-radius (Number)
height (Number)
shadow-color (Color)
shadow-offset (Offset)
shadow-opacity (Number)
shadow-radius (Number)
width (Number)
Window

UIWindow

background-color (Color)

Style Value Types

Boolean - A boolean (true or false)
BorderStyle - A border style, as rendered by a UITextBorderStyle. Accepted values are none, line, bezel, and rounded.
Box - A series of 1 to 4 integers that specify the widths of a box's edges. Interpreted like CSS's padding and margin properties (top, right, bottom, left). Examples: 15 (a box with a width of 15 for each edge), 10 15 (a box with a width of 10 for the top and bottom edges and 15 for the right and left edges)
Color - A hex color (e.g. #FF0000); a rgb, rgba, hsl, or hsla expression (e.g. rgb(255,0,0) or hsla(0.5, 0, 1.0, 0.5)); or a color name that UIColor has a related method name for (e.g. red, yellow, clear). If [UIColor redColor] is supported, then red is supported.
FontName - A font name. See available values here. Can also be system, boldSystem or italicSystem.
Gradient - Two Colors that will create a vertical gradient. background-color-top and background-color-bottom need to be defined in separate .nss properties.
Image - A name of an image, as used in [UIImage imageNamed:name] (e.g. MyImage.png).
Number - A number (e.g. -1, 4.5)
Offset - Two numbers comprising the horizontal and vertical values of an offset (e.g. -1,1)
SeparatorStyle - A separator style, as rendered by a UITableViewSeparatorStyle. Accepted values are none, single-line, and single-line-etched.
TextAlign - A text alignment (e.g. left, right, center)
TextTransform - A text transform (e.g. uppercase, lowercase, capitalize, none)
VerticalAlign - A vertical alignment (e.g. top, center, bottom, fill)

#backup
@primaryFontName: ArialRoundedMTBold;
@secondaryFontName: ArialRoundedMTBold;
@secondaryFontNameBold: ArialRoundedMTBold;
@secondaryFontNameStrong: ArialRoundedMTBold;
@inputFontName: ArialRoundedMTBold;
@primaryFontColor: #666666;
@secondaryFontColor: #888888;
@navFontColor: #FFFFFF;
@primaryBackgroundColor: #E6E6E6;
@primaryBackgroundTintColor: #ECECEC;
@primaryBackgroundColorTop: #FFFFFF;
@primaryBackgroundColorBottom: #ECECEC;
@primaryBackgroundColorBottomStrong: #DDDDDD;
@secondaryBackgroundColorTop: #FFFFFF;
@secondaryBackgroundColorBottom: #F9F9F9;
@primaryBorderColor: #EEEEEE;
@primaryBorderWidth: 1;

BarButton {
background-color: #9ED5F5;
corner-radius: 7;
font-name: @secondaryFontNameBold;
font-color: @navFontColor;
font-size: 13;
text-shadow-color: clear;
}
Button {
background-color: #FFFFFF;
border-color: @primaryBorderColor;
border-width: @primaryBorderWidth;
font-color: @primaryFontColor;
font-color-highlighted: @secondaryFontColor;
font-name: @secondaryFontName;
font-size: 16;
height: 37;
exclude-views: UIAlertButton;
exclude-subviews: UITableViewCell,UITextField;
}
SubmitButton {
background-color: #FFD740;
corner-radius: 3;
}
CancelButton{
background-color: #EFEFEF;
}
Label {
font-name: @secondaryFontName;
font-size: 20;
font-color: @primaryFontColor;
text-auto-fit: false;
}
LargeLabel {
font-size: 28;
}
SmallLabel {
font-size: 15;
}
NavigationBar {
font-name: @secondaryFontName;
font-size: 20;
font-color: @navFontColor;
text-shadow-color: clear;
background-color-top: #54B4EB;
background-color-bottom: #2FA4E7;
}
SearchBar {
background-color-top: @primaryBackgroundColorTop;
background-color-bottom: @primaryBackgroundColorBottom;
scope-background-color: #FFFFFF;
}
SegmentedControl {
background-color: @primaryBackgroundColorTop;
background-color-selected: #EEEEEE;
border-color: #999999;
border-width: @primaryBorderWidth;
corner-radius: 7;
font-name: @secondaryFontNameBold;
font-size: 13;
font-color: @primaryFontColor;
text-shadow-color: clear;
}
Switch {
on-tint-color: @primaryBackgroundTintColor;
}
TabBar {
background-color: #FFFFFF;
}
TabBarItem {
font-name: @secondaryFontName;
font-color: @primaryFontColor;
font-size: 18;
text-offset: 0,-11;
}
TableCell {
background-color-top: @secondaryBackgroundColorTop;
background-color-bottom: @secondaryBackgroundColorBottom;
font-color: @primaryFontColor;
font-name: @secondaryFontNameBold;
font-size: 17;
}
TableCellDetail {
font-name: @secondaryFontName;
font-size: 14;
font-color: @secondaryFontColor;
}
TextField {
height: 37;
font-name: @inputFontName;
font-color: @primaryFontColor;
font-size: 18;
padding: 0 10 0 10;
border-style: rounded;
border-width: @primaryBorderWidth;
border-color: @primaryBorderColor;
corner-radius: 4;
vertical-align: center;
}
LargeTextField {
height: 50;
}
LargeTextField {
font-size: 28;
}
View {
}
