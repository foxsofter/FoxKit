//
//  UILabel+Style.m
//  FoxKit
//
//  Created by fox softer on 15/10/15.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UILabel+Style.h"
#import "UILabel+NUI.h"

@implementation UILabel (Style)

+ (instancetype)labelWithPrimaryStyle {
  UILabel* label = [[UILabel alloc] init];
  label.nuiClass = @"PrimaryLabel";
  [label applyNUI];
  return label;
}
+ (instancetype)labelWithSecondaryStyle {
  UILabel* label = [[UILabel alloc] init];
  label.nuiClass = @"SecondaryLabel";
  [label applyNUI];
  return label;
}

+ (instancetype)labelWithPrimaryTipStyle {
  UILabel* label = [[UILabel alloc] init];
  label.nuiClass = @"PrimaryTipLabel";
  [label applyNUI];
  return label;
}

+ (instancetype)labelWithSecondaryTipStyle {
  UILabel* label = [[UILabel alloc] init];
  label.nuiClass = @"SecondaryTipLabel";
  [label applyNUI];
  return label;
}

@end
