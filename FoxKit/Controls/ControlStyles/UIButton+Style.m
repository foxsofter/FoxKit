//
//  UIButton+Style.m
//  FoxKit
//
//  Created by fox softer on 15/9/25.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UIButton+Style.h"
#import "UIButton+NUI.h"

@implementation UIButton (Style)

+(instancetype)buttonWithNormalStyle {
  UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
  [button applyNUI];
  return button;
}

+ (instancetype)buttonWithSubmitStyle {
  UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
  button.nuiClass = @"Button:SubmitButton";
  [button applyNUI];
  return button;
}

+ (instancetype)buttonWithCancelStyle {
  UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
  button.nuiClass = @"Button:CancelButton";
  [button applyNUI];
  return button;
}

@end
