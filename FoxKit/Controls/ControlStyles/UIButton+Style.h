//
//  UIButton+Style.h
//  FoxKit
//
//  Created by fox softer on 15/9/25.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @author foxsofter, 15-09-25 14:09:54
 *
 *  @brief  get buttons with different style .
 */
@interface UIButton (Style)

/**
 *  @author foxsofter, 15-09-25 14:09:49
 *
 *  @brief  get button of normal style
 *
 *  @return return normal style button
 */
+(instancetype)buttonWithNormalStyle;

/**
 *  @author foxsofter, 15-09-25 14:09:49
 *
 *  @brief  get button of submit style
 *
 *  @return return submit style button
 */
+(instancetype)buttonWithSubmitStyle;

/**
 *  @author foxsofter, 15-10-09 21:10:31
 *
 *  @brief  get button of cancel style
 *
 *  @return cancel style button
 */
+(instancetype)buttonWithCancelStyle;

@end
