//
//  UILabel+Style.h
//  FoxKit
//
//  Created by fox softer on 15/10/15.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

// Label {
//  font-size: 18;
//  font-name: @primaryFontName;
//  font-color: @primaryFontColor;
//  text-auto-fit: false;
//}
@interface UILabel (Style)

// PrimaryLabel {
//  font-name: @primaryFontName;
//  font-color: @primaryFontColor;
//}
+ (instancetype)labelWithPrimaryStyle;

// SecondaryLabel {
//  font-name: @secondaryFontName;
//  font-color: @secondaryFontColor;
//}
+ (instancetype)labelWithSecondaryStyle;

// PrimaryTipLabel {
//  font-name: @primaryTipFontName;
//  font-color: @primaryTipFontColor;
//}
+ (instancetype)labelWithPrimaryTipStyle;

// SecondaryTipLabel {
//  font-name: @secondaryTipFontName;
//  font-color: @secondaryTipFontColor;
//}
+ (instancetype)labelWithSecondaryTipStyle;

@end
