//
//  FXRefreshControl.m
//  DDPanGu
//
//  Created by wei on 15/4/23.
//  Copyright (c) 2015年 ddzg. All rights reserved.
//

#import "FXRefreshControl.h"
#import "UIControl+Block.h"
#import "UIView+Accessor.h"
#import "UIScrollView+Accessor.h"

@interface FXRefreshControl ()

/**
 *  @author foxsofter, 15-04-23 16:04:45
 *
 *  @brief  UIRefreshControl自身的UILabel在第一次的时候不显示文字，只能覆盖一个
 */
@property(nonatomic, strong) UILabel *refreshingTitleLabel;

@end

@implementation FXRefreshControl

- (instancetype)init {
  self = [super init];
  if (self) {
    _needRefreshing = NO;
    self.layer.zPosition = -1;
    self.tintColor = [UIColor grayColor];

    if (!self.refreshingTitleLabel) {
      self.refreshingTitleLabel = [[UILabel alloc] init];
    };

    [self valueChanged:^{
      if (self.refreshingTitle) {
        UILabel *titleLabel = ((UILabel *)((UIView *)self.subviews.firstObject)
                                   .subviews.lastObject);
        UIView *spinnerView = ((UIView *)((UIView *)self.subviews.firstObject)
                                   .subviews.firstObject);
        self.refreshingTitleLabel.text = self.refreshingTitle;
        self.refreshingTitleLabel.font = titleLabel.font;
        self.refreshingTitleLabel.textColor = titleLabel.textColor;
        self.refreshingTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.refreshingTitleLabel.frame = CGRectMake(
            0, spinnerView.frame.origin.y + spinnerView.frame.size.height, 200,
            16);

        [self addSubview:self.refreshingTitleLabel];

        static NSString *const Vertical =
            @"V:[spinnerView]-(-30)-[_refreshingTitleLabel(==16)]";
        static NSString *const Horizontal = @"H:|-[_refreshingTitleLabel]-|";

        NSDictionary *views =
            NSDictionaryOfVariableBindings(_refreshingTitleLabel, spinnerView);
        [self.refreshingTitleLabel
            setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.refreshingTitleLabel.superview
            addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:Vertical
                                                   options:0
                                                   metrics:nil
                                                     views:views]];
        [self.refreshingTitleLabel.superview
            addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:Horizontal
                                                   options:0
                                                   metrics:nil
                                                     views:views]];

        titleLabel.hidden = YES;
      }
      if (self.needRefreshing) {
        [self beginRefreshing];
      }
    }];
  }
  return self;
}

- (void)refreshingIfNeeded {
  UIScrollView *scrollView = (UIScrollView *)self.superview;
  if (_needRefreshing && !self.isRefreshing) {
    [scrollView bringSubviewToFront:self];
    CGPoint origin = scrollView.contentOffset;
    [scrollView
        setContentOffset:CGPointMake(0, origin.y - self.frame.size.height)
                animated:YES];
  }
  [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)endRefreshing {
  self.needRefreshing = NO;
  UIScrollView *scrollView = (UIScrollView *)self.superview;
  [scrollView sendSubviewToBack:self];
  self.layer.zPosition = 10000;
  if (self.refreshingTitle) {
    UILabel *titleLabel =
        ((UILabel *)((UIView *)self.subviews.firstObject).subviews.lastObject);

    [self.refreshingTitleLabel removeFromSuperview];

    titleLabel.hidden = NO;
  }

  [super endRefreshing];
}

@end
