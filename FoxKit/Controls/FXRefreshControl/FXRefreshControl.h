//
//  FXRefreshControl.h
//  DDPanGu
//
//  Created by wei on 15/4/23.
//  Copyright (c) 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXRefreshControl : UIRefreshControl

/**
 *  @author foxsofter, 15-04-23 11:04:05
 *
 *  @brief  是否需要触发刷新事件
 */
@property(nonatomic, assign, getter=isNeedRefreshing) BOOL needRefreshing;

/**
 *  @author foxsofter, 15-04-23 11:04:30
 *
 *  @brief  检查是否需要刷新
 */
- (void)refreshingIfNeeded;

/**
 *  @author foxsofter, 15-04-23 14:04:28
 *
 *  @brief  refreshing title
 */
@property(nonatomic, copy) NSString *refreshingTitle;

@end
