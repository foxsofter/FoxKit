//
//  UIScreen+Accessor.h
//  FoxKit
//
//  Created by fox softer on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (Accessor)

+ (CGRect)bounds;

/**
 *  @author foxsofter, 15-09-23 22:09:40
 *
 *  @brief  主屏幕的size
 *
 *  @return size
 */
+ (CGSize)size;

/**
 *  @author foxsofter, 15-09-23 22:09:11
 *
 *  @brief  主屏幕的width
 *
 *  @return width
 */
+ (CGFloat)width;

/**
 *  @author foxsofter, 15-09-23 22:09:30
 *
 *  @brief  主屏幕的height
 *
 *  @return height
 */
+ (CGFloat)height;

/**
 *  @author foxsofter, 15-09-23 22:09:18
 *
 *  @brief  当前屏幕的size
 */
@property(nonatomic, readonly) CGSize size;

/**
 *  @author foxsofter, 15-09-23 22:09:37
 *
 *  @brief  当前屏幕的width
 */
@property(nonatomic, readonly) CGFloat width;

/**
 *  @author foxsofter, 15-09-23 22:09:49
 *
 *  @brief  当前屏幕的height
 */
@property(nonatomic, readonly) CGFloat height;

/**
 *  @author foxsofter, 15-09-23 22:09:03
 *
 *  @brief  主屏幕的中点
 *
 *  @return CGPoint
 */
+ (CGPoint)midpoint;

/**
 *  @author foxsofter, 15-09-23 22:09:41
 *
 *  @brief  主屏幕中点到左边的距离
 *
 *  @return CGFloat
 */
+ (CGFloat)midpointX;

/**
 *  @author foxsofter, 15-09-23 22:09:31
 *
 *  @brief  主屏幕中点到顶部的距离
 *
 *  @return CGFloat
 */
+ (CGFloat)midpointY;

/**
 *  @author foxsofter, 15-09-23 22:09:03
 *
 *  @brief  当前屏幕的中点
 */
@property(nonatomic, readonly) CGPoint midpoint;

/**
 *  @author foxsofter, 15-09-23 22:09:08
 *
 *  @brief  当前屏幕中点到左边的距离
 */
@property(nonatomic, readonly) CGFloat midpointX;

/**
 *  @author foxsofter, 15-09-23 22:09:12
 *
 *  @brief  当前屏幕中点到顶部的距离
 */
@property(nonatomic, readonly) CGFloat midpointY;

/**
 *  @author foxsofter, 15-09-23 22:09:19
 *
 *  @brief  状态栏的高度
 *
 *  @return CGFloat
 */
+ (CGFloat)statusBarHeight;

/**
 *  @author foxsofter, 15-09-23 22:09:28
 *
 *  @brief  导航栏的高度
 *
 *  @return CGFloat
 */
+ (CGFloat)navigationBarHeight;

/**
 *  @author foxsofter, 15-09-23 22:09:32
 *
 *  @brief  工具栏的高度
 *
 *  @return CGFloat
 */
+ (CGFloat)toolBarHeight;

/**
 *  @author foxsofter, 15-09-23 22:09:36
 *
 *  @brief  tab菜单的高度
 *
 *  @return CGFloat
 */
+ (CGFloat)tabBarHeight;

@end
