//
//  TTTAttributedLabel+Addition.m
//  FoxKit
//
//  Created by fox softer on 15/10/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "TTTAttributedLabel+Addition.h"
#import "UIView+Accessor.h"

@implementation TTTAttributedLabel (Addition)

- (CGFloat)heightForString:(NSString *)text {
  NSMutableAttributedString *attrString =
      [[NSMutableAttributedString alloc] initWithString:text];

  NSMutableParagraphStyle *paragrapStyle =
      [[NSMutableParagraphStyle alloc] init];
  [paragrapStyle setLineSpacing:self.lineSpacing];
  [attrString addAttribute:NSParagraphStyleAttributeName
                     value:paragrapStyle
                     range:NSMakeRange(0, text.length)];
  //  设置字体
  [attrString addAttribute:NSFontAttributeName
                     value:self.font
                     range:NSMakeRange(0, text.length)];
  //  得到自定义行间距的UILabel的高度
  CGFloat height =
      [TTTAttributedLabel sizeThatFitsAttributedString:attrString
                                       withConstraints:CGSizeMake(self.width, MAXFLOAT)
                                limitedToNumberOfLines:0]
          .height;
  return height;
}

+(CGFloat)heightForString:(NSString *)text width:(CGFloat)width font:(UIFont *)font lineSpacing:(CGFloat)lineSpacing {
  NSMutableAttributedString *attrString =
  [[NSMutableAttributedString alloc] initWithString:text];
  
  NSMutableParagraphStyle *paragrapStyle =
  [[NSMutableParagraphStyle alloc] init];
  [paragrapStyle setLineSpacing:lineSpacing];
  [attrString addAttribute:NSParagraphStyleAttributeName
                     value:paragrapStyle
                     range:NSMakeRange(0, text.length)];
    //  设置字体
  [attrString addAttribute:NSFontAttributeName
                     value:font
                     range:NSMakeRange(0, text.length)];
    //  得到自定义行间距的UILabel的高度
  CGFloat height =
  [TTTAttributedLabel sizeThatFitsAttributedString:attrString
                                   withConstraints:CGSizeMake(width, MAXFLOAT)
                            limitedToNumberOfLines:0]
  .height;
  return height;
}

@end
