//
//  UIImage+Addition.h
//  FoxKit
//
//  Created by fox softer on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Addition)

/**
 *  @author foxsofter, 15-09-23 22:09:43
 *
 *  @brief  以传入的视图为源，根据主窗口压缩比例截图
 *
 *  @param sourceView 源视图
 *
 *  @return 如果源视图为nil，则返回nil
 */
+ (UIImage *)createImageFromView:(UIView *)sourceView;

/**
 *  @author foxsofter, 15-09-23 22:09:05
 *
 *  @brief  根据颜色生成图片，默认size为{1.f, 1.f}
 *
 *  @param color 传入颜色
 *
 *  @return 返回图片
 */
+ (UIImage *)createImageWithColor:(UIColor *)color;

/**
 *  @author foxsofter, 15-09-23 22:09:50
 *
 *  @brief  根据颜色和传入的size生成图片
 *
 *  @param color 传入的颜色
 *  @param size  生成图片的size
 *
 *  @return 返回图片
 */
+ (UIImage *)createImageWithColor:(UIColor *)color size:(CGSize)size;

@end
