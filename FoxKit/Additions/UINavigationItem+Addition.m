//
//  UINavigationItem+Addition.m
//  FoxKit
//
//  Created by fox softer on 15/10/10.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UINavigationItem+Addition.h"
#import "NSObject+Swizzling.h"

@implementation UINavigationItem (Addition)

+ (void)load {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    SEL oldSelector = @selector(backBarButtonItem);
    SEL newSelector = @selector(ws_backBarButtonItem);

    [self instanceSwizzle:oldSelector newSelector:newSelector];
  });
}

- (UIBarButtonItem *)ws_backBarButtonItem {
  return [[UIBarButtonItem alloc] initWithTitle:@""
                                          style:UIBarButtonItemStylePlain
                                         target:nil
                                         action:NULL];
}

@end
