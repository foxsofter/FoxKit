//
//  UIColor+Addition.h
//  WisdomAir
//
//  Created by wei on 15/1/19.
//  Copyright (c) 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIColor.h>

#define COLOR_IMPL_FROM_STRING(colorName, colorString)     \
  +(UIColor *)colorName##Color {                           \
    static UIColor *color = nil;                           \
    if (!color) {                                          \
      color = [UIColor colorWithHexString:@ #colorString]; \
    }                                                      \
    return color;                                          \
  }

#define COLOR_IMPL_FROM_VALUE(colorName, colorValue) \
  +(UIColor *)colorName##Color {                     \
    static UIColor *color = nil;                     \
    if (!color) {                                    \
      color = colorValue;                            \
    }                                                \
    return color;                                    \
  }

@interface UIColor (Addition)

+ (UIColor *)colorWithHexString:(NSString *)hexString;


/**
 *  @author foxsofter, 15-10-17 17:10:54
 *
 *  @brief  获取当前颜色的反色，即补色，颜色和其反色混合的效果是白色
 *          如果获取当前颜色有异常，将返回白色
 *
 *  @return 当前颜色的反色
 */
- (UIColor*)antiColor;


@end