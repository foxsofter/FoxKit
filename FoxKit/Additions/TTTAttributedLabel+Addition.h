//
//  TTTAttributedLabel+Addition.h
//  FoxKit
//
//  Created by fox softer on 15/10/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

@import Foundation;
#import "TTTAttributedLabel.h"

@interface TTTAttributedLabel (Addition)

-(CGFloat)heightForString:(NSString*)text;

+(CGFloat)heightForString:(NSString*)text width:(CGFloat)width font:(UIFont*)font lineSpacing:(CGFloat)lineSpacing;

@end
