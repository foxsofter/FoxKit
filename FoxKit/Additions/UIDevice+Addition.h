//
//  UIDevice+Addition.h
//  FoxKit
//
//  Created by fox softer on 15/10/31.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Addition)

+ (NSString *)platform;
+ (NSString *)platformString;

@end
