//
//  UIView+Borders.h
//  FoxKit
//
//  Created by fox softer on 15/10/21.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, BorderType) {
  BorderTypeSolid,
  BorderTypeDashed,
  BorderTypeDoted
};

typedef NS_OPTIONS(NSUInteger, BorderPosition) {
  BorderPositionNone = 0,
  BorderPositionTop = 1 << 0,
  BorderPositionRight = 1 << 1,
  BorderPositionBottom = 1 << 2,
  BorderPositionLeft = 1 << 3,
};

/**
 *  @author foxsofter, 15-10-21 16:10:11
 *
 *  @brief  灵活设置border，为了不影响视觉，并不允许设置不同border为不同颜色
 */
@interface UIView (Borders)

/**
 *  @author foxsofter, 15-10-21 16:10:16
 *
 *  @brief  设置UIView的border,绘制所有border
 *
 *  @param borderWidth borderWidth
 *  @param borderColor borderColor
 */
- (void)setBorder:(CGFloat)borderWidth borderColor:(UIColor*)borderColor;

/**
 *  @author foxsofter, 15-10-21 16:10:35
 *
 *  @brief  设置UIView的border，根据borderPosition设置对应的border
 *
 *  @param borderWidth    borderWidth
 *  @param borderColor    borderColor
 *  @param borderPosition borderPosition
 */
- (void)setBorder:(CGFloat)borderWidth
      borderColor:(UIColor*)borderColor
   borderPosition:(BorderPosition)borderPosition;

/**
 *  @author foxsofter, 15-10-21 16:10:06
 *
 *  @brief  设置UIView的border,根据borderType绘制所有border
 *
 *  @param borderWidth borderWidth
 *  @param borderColor borderColor
 *  @param borderType  borderType
 */
- (void)setBorder:(CGFloat)borderWidth
      borderColor:(UIColor*)borderColor
       borderType:(BorderType)borderType;

/**
 *  @author foxsofter, 15-10-21 16:10:22
 *
 *  @brief  设置UIView的border,根据borderType和borderPosition绘制border
 *
 *  @param borderWidth    borderWidth
 *  @param borderColor    borderColor
 *  @param borderPosition borderPosition
 *  @param borderType     borderType
 */
- (void)setBorder:(CGFloat)borderWidth
      borderColor:(UIColor*)borderColor
   borderPosition:(BorderPosition)borderPosition
       borderType:(BorderType)borderType;

@end
