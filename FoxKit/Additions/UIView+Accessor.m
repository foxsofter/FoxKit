//
//  UIView+Accessor.m
//  FoxKit
//
//  Created by fox softer on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UIView+Accessor.h"

@implementation UIView (Accessor)

#pragma mark - Frame Origin

- (CGPoint)origin {
  return self.frame.origin;
}

- (void)setOrigin:(CGPoint)newOrigin {
  CGRect newFrame = self.frame;
  newFrame.origin = newOrigin;
  self.frame = newFrame;
}

- (CGFloat)x {
  return self.frame.origin.x;
}

- (void)setX:(CGFloat)newX {
  CGRect newFrame = self.frame;
  newFrame.origin.x = newX;
  self.frame = newFrame;
}

- (CGFloat)y {
  return self.frame.origin.y;
}

- (void)setY:(CGFloat)newY {
  CGRect newFrame = self.frame;
  newFrame.origin.y = newY;
  self.frame = newFrame;
}

#pragma mark - Frame Size

- (CGSize)size {
  return self.bounds.size;
}

- (void)setSize:(CGSize)newSize {
  CGRect newFrame = self.frame;
  newFrame.size = newSize;
  self.frame = newFrame;
}

- (CGFloat)height {
  return self.frame.size.height;
}

- (void)setHeight:(CGFloat)newHeight {
  CGRect newFrame = self.frame;
  newFrame.size.height = newHeight;
  self.frame = newFrame;
}

- (CGFloat)width {
  return self.frame.size.width;
}

- (void)setWidth:(CGFloat)newWidth {
  CGRect newFrame = self.frame;
  newFrame.size.width = newWidth;
  self.frame = newFrame;
}

#pragma mark - Frame bounds

- (CGFloat)top {
  return self.bounds.origin.y - self.frame.origin.y;
}

- (void)setTop:(CGFloat)top {
  CGRect newBounds = self.bounds;
  newBounds.origin.y = self.frame.origin.y + top;
  self.bounds = newBounds;
}

- (CGFloat)left {
  return self.bounds.origin.x - self.frame.origin.x;
}

- (void)setLeft:(CGFloat)left {
  CGRect newBounds = self.bounds;
  newBounds.origin.x = self.frame.origin.x + left;
  self.bounds = newBounds;
}

- (CGFloat)right {
  return (self.frame.origin.x + self.frame.size.width) - (self.bounds.origin.x + self.bounds.size.width);
}

- (void)setRight:(CGFloat)right {
  CGRect newBounds = self.bounds;
  newBounds.size.width = self.frame.size.width - right;
  self.bounds = newBounds;
}

- (CGFloat)bottom {
  return (self.frame.origin.y + self.frame.size.height) - (self.bounds.origin.y + self.bounds.size.height);
}

- (void)setBottom:(CGFloat)bottom {
  CGRect newBounds = self.bounds;
  newBounds.size.height = self.frame.size.width - bottom;
  self.bounds = newBounds;
}

#pragma mark - Center Point

- (CGFloat)centerX {
  return self.center.x;
}

- (void)setCenterX:(CGFloat)newCenterX {
  self.center = CGPointMake(newCenterX, self.center.y);
}

- (CGFloat)centerY {
  return self.center.y;
}

- (void)setCenterY:(CGFloat)newCenterY {
  self.center = CGPointMake(self.center.x, newCenterY);
}

#pragma mark - Middle Point

- (CGPoint)midpoint {
  return CGPointMake(self.midpointX, self.midpointY);
}

- (CGFloat)midpointX {
  return self.bounds.origin.x + self.bounds.size.width / 2;
}

- (CGFloat)midpointY {
  return self.bounds.origin.y + self.bounds.size.height / 2;
}

@end
