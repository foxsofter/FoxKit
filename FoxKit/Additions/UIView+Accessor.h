//
//  UIView+Accessor.h
//  FoxKit
//
//  Created by foxsofter on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @author foxsofter, 15-09-24 09:09:51
 *
 *  @brief  View layout: 包含相对于父视图的布局和相对于自身的布局
 *          frame layout: 相对于父视图的布局，为视图占据父视图的部分
 *          bounds layout: 相对于自身的布局，为视图的可布局子视图的部分
 *           ______________________
 *          | ____________________ |
 *          | |                  | |
 *          | |      bounds      | |
 *          | |                  | |
 *          | |__________________| |
 *          |______________________|
 *
 *                   frame
 *
 */
@interface UIView (Accessor)

/**
 *  @author foxsofter, 15-09-23 22:09:11
 *
 *  @brief  get view.frame.origin
 */
@property(nonatomic) CGPoint origin;

/**
 *  @author foxsofter, 15-09-23 22:09:19
 *
 *  @brief  get view.frame.origin.x
 */
@property(nonatomic) CGFloat x;

/**
 *  @author foxsofter, 15-09-23 22:09:26
 *
 *  @brief  get view.frame.origin.y
 */
@property(nonatomic) CGFloat y;

/**
 *  @author foxsofter, 15-09-23 22:09:15
 *
 *  @brief  get view.frame.size
 */
@property(nonatomic) CGSize size;

/**
 *  @author foxsofter, 15-09-23 22:09:35
 *
 *  @brief  get view.frame.size.width
 */
@property(nonatomic) CGFloat width;

/**
 *  @author foxsofter, 15-09-23 22:09:40
 *
 *  @brief  get view.frame.size.height
 */
@property(nonatomic) CGFloat height;

/**
 *  @author foxsofter, 15-09-23 22:09:46
 *
 *  @brief  get view.bounds.origin.y - view.bounds.origin.y
 */
@property(nonatomic) CGFloat top;

/**
 *  @author foxsofter, 15-09-23 22:09:50
 *
 *  @brief  get view.bounds.origin.x - view.bounds.origin.x
 */
@property(nonatomic) CGFloat left;

/**
 *  @author foxsofter, 15-09-23 22:09:53
 *
 *  @brief  get (view.frame.size.height + view.frame.origin.y) 
 *              -
 *              (view.bounds.size.height + view.bounds.origin.y)
 */
@property(nonatomic) CGFloat bottom;

/**
 *  @author foxsofter, 15-09-23 22:09:57
 *
 *  @brief  get (view.frame.size.width + view.frame.origin.x)
 *              -
 *              (view.bounds.size.width + view.bounds.origin.x)
 */
@property(nonatomic) CGFloat right;

/**
 *  @author foxsofter, 15-09-23 22:09:02
 *
 *  @brief  get view.center.x
 */
@property(nonatomic) CGFloat centerX;

/**
 *  @author foxsofter, 15-09-23 22:09:05
 *
 *  @brief  get view.center.y
 */
@property(nonatomic) CGFloat centerY;

/**
 *  @author foxsofter, 15-09-23 22:09:11
 *
 *  @brief  get { 
 *                view.bouds.origin.x + view.bounds.size.width / 2,
 *                view.bouds.origin.y + view.bounds.size.height / 2
 *              }
 */
@property(nonatomic, readonly) CGPoint midpoint;

/**
 *  @author foxsofter, 15-09-23 22:09:15
 *
 *  @brief  get view.bounds.origin.x + view.bounds.size.width / 2
 */
@property(nonatomic, readonly) CGFloat midpointX;

/**
 *  @author foxsofter, 15-09-23 22:09:19
 *
 *  @brief  get view.bounds.origin.y + view.bounds.size.height / 2
 */
@property(nonatomic, readonly) CGFloat midpointY;

@end
