//
//  UIScrollView+Accessor.h
//  FoxKit
//
//  Created by fox softer on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Accessor)

/**
 *  @author foxsofter, 15-09-23 23:09:10
 *
 *  @brief  get contentOffset.x
 */
@property(nonatomic) CGFloat contentOffsetX;

/**
 *  @author foxsofter, 15-09-23 23:09:21
 *
 *  @brief  get contentOffset.y
 */
@property(nonatomic) CGFloat contentOffsetY;

/**
 *  @author foxsofter, 15-09-23 23:09:31
 *
 *  @brief  get contentSize.width
 */
@property(nonatomic) CGFloat contentSizeWidth;

/**
 *  @author foxsofter, 15-09-23 23:09:45
 *
 *  @brief  get contentSize.height
 */
@property(nonatomic) CGFloat contentSizeHeight;

/**
 *  @author foxsofter, 15-09-23 23:09:01
 *
 *  @brief  get contentInset.top
 */
@property(nonatomic) CGFloat contentInsetTop;

/**
 *  @author foxsofter, 15-09-23 23:09:25
 *
 *  @brief  get contentInset.left
 */
@property(nonatomic) CGFloat contentInsetLeft;

/**
 *  @author foxsofter, 15-09-23 23:09:36
 *
 *  @brief  get contentInset.bottom
 */
@property(nonatomic) CGFloat contentInsetBottom;

/**
 *  @author foxsofter, 15-09-23 23:09:49
 *
 *  @brief  get contentInset.right
 */
@property(nonatomic) CGFloat contentInsetRight;

@end
