//
//  UIScreen+Accessor.m
//  FoxKit
//
//  Created by fox softer on 15/9/23.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import "UIScreen+Accessor.h"

@implementation UIScreen (Accessor)

#pragma mark - Size

+ (CGRect)bounds {
  return UIScreen.mainScreen.bounds;
}

+ (CGSize)size {
  return UIScreen.mainScreen.size;
}

+ (CGFloat)width {
  return UIScreen.size.width;
}

+ (CGFloat)height {
  return UIScreen.size.height;
}

- (CGSize)size {
  return self.bounds.size;
}

- (CGFloat)height {
  return self.size.height;
}

- (CGFloat)width {
  return self.size.width;
}

#pragma mark - Middle Point

+ (CGPoint)midpoint {
  return UIScreen.mainScreen.midpoint;
}

+ (CGFloat)midpointX {
  return UIScreen.mainScreen.midpointX;
}

+ (CGFloat)midpointY {
  return UIScreen.mainScreen.midpointY;
}

- (CGPoint)midpoint {
  return CGPointMake(self.midpointX, self.midpointY);
}

- (CGFloat)midpointX {
  return self.width / 2;
}

- (CGFloat)midpointY {
  return self.height / 2;
}

#pragma mark - iphone ui design value

+ (CGFloat)statusBarHeight {
  return 20.f;
}

+ (CGFloat)navigationBarHeight {
  return 64.f;
}

+ (CGFloat)toolBarHeight {
  return 44;
}

+ (CGFloat)tabBarHeight {
  return 49;
}

@end
