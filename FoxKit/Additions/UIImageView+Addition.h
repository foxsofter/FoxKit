//
//  UIImageView+Addition.h
//  FoxKit
//
//  Created by fox softer on 15/10/21.
//  Copyright © 2015年 foxsofter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Addition)

/**
 *  @author foxsofter, 15-10-22 23:10:39
 *
 *  @brief  获取缓存的图像
 *
 *  @param url remote url
 *
 *  @return 
 */
+(UIImage*)cacheImageFromUrl:(NSString*)url;

/**
 *  @author foxsofter, 15-10-22 16:10:54
 *
 *  @brief  设置图像
 *
 *  @param url
 */
- (void)setImageUrl:(NSString *)url;

/**
 *  @author foxsofter, 15-10-22 16:10:14
 *
 *  @brief  设置图像
 *
 *  @param url remote url
 */
- (void)setImageUrl:(NSString *)url withDuration:(NSTimeInterval)duration;
/**
 *  @author foxsofter, 15-10-21 10:10:34
 *
 *  @brief  设置图像，并将其设为圆形，default
 *          borderWidth：1，borderColor：whiteColor
 *
 *  @param url              image url
 *  @param placeholderImage placeholder Image
 */
- (void)setCircleImageWithURL:(NSURL *)url
             placeholderImage:(nullable UIImage *)placeholderImage;

/**
 *  @author foxsofter, 15-10-21 10:10:18
 *
 *  @brief  设置图像，并将其设为圆形
 *
 *  @param url              image url
 *  @param placeholderImage placeholder image
 *  @param borderWidth      border width
 *  @param borderColor      border color
 */
- (void)setCircleImageWithURL:(NSURL *)url
             placeholderImage:(nullable UIImage *)placeholderImage
                  borderWidth:(CGFloat)borderWidth
                  borderColor:(UIColor *)borderColor;

/**
 *  @author foxsofter, 15-10-21 10:10:09
 *
 *  @brief  设置图像，并将其设为圆形
 *
 *  @param url              image url
 *  @param placeholderImage placeholder Image
 *  @param showProgress     showProgress
 */
- (void)setCircleImageWithURL:(NSURL *)url
             placeholderImage:(nullable UIImage *)placeholderImage
                 showProgress:(BOOL)showProgress;

/**
 *  @author foxsofter, 15-10-21 10:10:51
 *
 *  @brief  设置图像，并将其设为圆形
 *
 *  @param url              image url
 *  @param placeholderImage placeholder Image
 *  @param borderWidth      border Width
 *  @param borderColor      border Color
 *  @param showProgress     show Progress
 */
- (void)setCircleImageWithURL:(NSURL *)url
             placeholderImage:(nullable UIImage *)placeholderImage
                  borderWidth:(CGFloat)borderWidth
                  borderColor:(UIColor *)borderColor
                 showProgress:(BOOL)showProgress;

@end
